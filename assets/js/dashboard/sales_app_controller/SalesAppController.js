/**
 * Created by Eric.Badger on 3/23/2017.
 */
HerbalTechControllers.controller('SalesAppController', ['$scope', '$http', 'cfpLoadingBar', '$location',function ($scope, $http, cfpLoadingBar, $location) {
    // set the correct navs
    eventEmitter({topNav: '/templates/sales/top_nav.html', activeTopNav: '', activeSideBar: 'sales_app'}, $scope);
    // eventEmitter({activeSideBar: 'product'}, $scope);
    cfpLoadingBar.start();

    $http.get('/product').then(function ok(res) {
        // console.log(res);
        $scope.products = res.data;
        console.log($scope.products)
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });
    $http.get('/product').then(function ok(res) {
        // console.log(res);
        $scope.products = res.data;
        console.log($scope.products)
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    //model for user setup
    $scope.product_setup_model = {
        product_info: {},
        distribute: {},
        batch: {}
    };

}]);