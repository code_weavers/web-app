/**
 * Created by Eric.Badger on 3/22/2017.
 */
HerbalTechControllers.controller('DashboardIndexController', ['$scope', '$http', 'cfpLoadingBar', 'SharedUserDetails', function ($scope, $http, cfpLoadingBar, SharedUserDetails) {
    $scope.role_weight = SharedUserDetails.get('role_weight');
    // set the correct navs
    if ($scope.role_weight > 2)
        eventEmitter({topNav: '/templates/dashboard/top_nav.html', activeTopNav: '', activeSideBar: 'dashboard'}, $scope);
    else
        eventEmitter({topNav: '', activeTopNav: '', activeSideBar: 'dashboard'}, $scope);
    // eventEmitter({activeSideBar: 'product'}, $scope);
    // cfpLoadingBar.start();

    $http.get('/dashboard/get_total').then(function ok(res) {
        // console.log(res);
        $scope.totals = res.data;
        console.log($scope.totals);
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    $http.get('/dashboard/total_product').then(function ok(res) {
        // console.log(res);
        $scope.total_products = res.data;
        console.log($scope.total_products);
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    //model for user setup
    $scope.dashboard_setup_model = {
        commission: {},
        serial: {}
    };

    $scope.new_commission = function () {
        // console.log($scope.user_setup_model);
        //let us make sure all required fields are filled
        var keys = ['commission', 'description'];
        for (var i = 0; i < keys.length; i++) {
            if (!(keys[i] in $scope.dashboard_setup_model.commission)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        //check username first
        cfpLoadingBar.start();
        //very thing is fine push data to server
        $http.post("/dashboard/new_commission", $scope.dashboard_setup_model.commission).then(function ok(res) {
            console.log(res);
            $('#myModalCommision').modal('hide');
            location.reload();
        }, function error(res) {
            if (res.status == 403) {
                window.location = "/";
            } else
                return $scope.flash_user_setup_model = res.data.msg;
        });
        // }, function error(res) {
        //     if (res.status == 403) {
        //         window.location = '/';
        //     } else
        //         return $scope.flash_user_setup_model = res.data.msg;
        // });

    };

    $scope.new_serial = function () {
        // console.log($scope.user_setup_model);
        //let us make sure all required fields are filled
        var keys = ['serial'];
        for (var i = 0; i < keys.length; i++) {
            if (!(keys[i] in $scope.dashboard_setup_model.serial)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        //check username first
        cfpLoadingBar.start();
        //very thing is fine push data to server
        $http.post("/dashboard/new_serial", $scope.dashboard_setup_model.serial).then(function ok(res) {
            console.log(res);
            $('#myModalSerialNo').modal('hide');
            location.reload();
        }, function error(res) {
            if (res.status == 403) {
                window.location = "/";
            } else
                return $scope.flash_user_setup_model = res.data.msg;
        });
        // }, function error(res) {
        //     if (res.status == 403) {
        //         window.location = '/';
        //     } else
        //         return $scope.flash_user_setup_model = res.data.msg;
        // });

    };

}]);
