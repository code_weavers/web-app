/**
 * Created by oteng on 3/1/17.
 */
//for receiving broadcast events and responding to it
HerbalTechControllers.controller('LayoutController', ['$scope', '$http', 'SharedUserDetails', function ($scope, $http, SharedUserDetails) {
    $http.get('/user/get_login_user').success(function (res) {
        $scope.user_first_name = res.user;
        $scope.role_name = res.role;
        SharedUserDetails.add('role_weight', res.role_weight)
    });

    //subscribe to nav change events
    //subscribe to controller change broadcast so that you can change the top nav
    $scope.$on('HT:layoutController:setActiveNavs', function (event, data) {
        if (data.topNav)
            $scope.topNav = data.topNav;
        if (data.activeTopNav)
            $scope.activeTopNav = data.activeTopNav;
        if (data.activeSideBar)
            $scope.activeSideBar = data.activeSideBar;
    });

}]);


