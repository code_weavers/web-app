/**
 * Created by oteng on 3/28/17.
 */
io.socket.on('connect', function () {
    io.socket.get('/realtime/notify');
    // io.socket.get('/api/notify');

});

io.socket.on('tbl_direct_cash', function onServerSentEvent(msg) {
    console.log(msg, "tbl_direct_cash");
    if (msg.verb == "created") {
        var html = '<div id="main_notification" class="alert alert-info alert-dismissible" role="alert"> ' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button> <strong>Direct Cash Sales:</strong> direct cash sales for</div>';
        $('notification_body').html(html);
    }
});
io.socket.on('tbl_deduction', function onServerSentEvent(msg) {
    console.log(msg, "Tbl_Deduction");
    if (msg.verb == "created") {
        var html = '<div id="main_notification" class="alert alert-info alert-dismissible" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button> <strong>New Deduction Request:</strong> A sales person is waiting confirmation</div>';
        $('notification_body').html(html);
    }
});
