/**
 * Created by oteng on 3/11/17.
 */
HerbalTechControllers.controller('UserDetailsController', ['$scope', '$http', 'cfpLoadingBar', '$routeParams', '$route', function ($scope, $http, cfpLoadingBar, $routeParams, $route) {

    // set the correct navs
    eventEmitter({topNav: '', activeTopNav: '', activeSideBar: 'users'}, $scope);
    var userID = $routeParams.userId;

    cfpLoadingBar.start();
    // console.log('loading users', userID)
    $http.get(('/user/get_user/' + userID)).then(function ok(res) {
        var date = (new Date(res.data.user.dob)).toDateString();
        $scope.user_details = res.data;
        $scope.user_details.user.dob_read = date;
        // console.log(res.data)
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    cfpLoadingBar.start();
    $http.get('/product/get_users_product/' + userID).then(function ok(res) {
        console.log(res.data);
        $scope.user_products = res.data
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    $scope.update_user_profile = function () {
        cfpLoadingBar.start();
        $http.post(('/user/update_user/' + $scope.user_details.user.id), $scope.user_details).then(function ok(res) {
            console.log(res);
        }, function error(res) {
            if (res.status == 403) {
                window.location = '/';
            }
        });
        // console.log($scope.user_details);
    };

    $scope.change_password = function () {
        if ($scope.pass_reset.new_pass !== $scope.pass_reset.confirm_new_pass) {
            return $scope.flash_user_change_pass_model = "the passwords don't much"
        } else {
            console.log("sending");
            cfpLoadingBar.start();
            $http.post(('/login/change_pass/' + $scope.user_details.user.id), $scope.pass_reset).then(function ok(res) {
                console.log(res);
                modal_hide("reset_pass_model")
            }, function error(res) {
                if (res.status == 403) {
                    window.location = '/';
                }
            });
        }
    };

    $scope.upload_profile_picture = function () {
        console.log($routeParams.userId);
        /* Using AJAX */
        var formData = new FormData(),
            xhr = new XMLHttpRequest();

        /* Required for large files */
        // xhr.setRequestHeader('X-CSRF-Token', csrfToken);
        var inputPix = document.getElementById('image_input');
        formData.append('pix', inputPix.files[0]);

        xhr.open('POST', '/user/upload/' + $routeParams.userId, true);
        xhr.send(formData);
        xhr.onload = function () {
            /* callback on upload */
            location.reload();
            // modal_hide("updatePicture");
            // $route.reload();
        };
    };

    $scope.datePopup = {
        a: false,
        b: false,
        c: false,
        d: false,
        e: false
    };

    $scope.openDatePopup = function (id) {
        $scope.datePopup[id] = true;
        // $scope.popup1.opened = true;
    };
}]);
