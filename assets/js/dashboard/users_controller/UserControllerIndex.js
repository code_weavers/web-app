/**
 * Created by oteng on 2/28/17.
 */
HerbalTechControllers.controller('UserControllerIndex', ['$scope', '$http', 'cfpLoadingBar', 'SharedUserDetails', '$location', '$route', function ($scope, $http, cfpLoadingBar, SharedUserDetails, $location, $route) {

    // set the correct navs
    eventEmitter({topNav: '/templates/users/top_nav.html', activeTopNav: '', activeSideBar: 'users'}, $scope);
    cfpLoadingBar.start();
    $http.get('/user').then(function ok(res) {
        // console.log(res);
        $scope.role_weight = SharedUserDetails.get('role_weight');
        $scope.users = res.data;
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    //model for user setup
    $scope.user_setup_model = {
        personal_data: {},
        contact: {},
        edu: {},
        emply: {},
        family: {},
        login: {}
    };


    $scope.create_user = function () {
        // console.log($scope.user_setup_model);
        //let us make sure all required fields are filled
        var keys = ['f_name', 'l_name', 'dob', 'hometown', 'nationality', 'hobbies', 'profession'];
        for (var i = 0; i < keys.length; i++) {
            if (!(keys[i] in $scope.user_setup_model.personal_data)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        var keys2 = ['phone'];
        for (var i = 0; i < keys2.length; i++) {
            if (!(keys2[i] in $scope.user_setup_model.contact)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        var keys3 = ['username', 'password', 'role'];
        for (var i = 0; i < keys3.length; i++) {
            if (!(keys3[i] in $scope.user_setup_model.login)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        //check username first
        cfpLoadingBar.start();
        $http.get('/user/check_username/' + $scope.user_setup_model.login.username).then(function ok(res) {
            // console.log(res);
            if (res.data.result) {
                return $scope.flash_user_setup_model = 'The user name has already been taken';
            }

            //very thing is fine push data to server
            $http.post('/user/create', $scope.user_setup_model).then(function ok(res) {

                console.log(res);
                modal_hide('add_user_model');
                $route.reload();

            }, function error(res) {
                if (res.status == 403) {
                    window.location = '/';
                } else
                    return $scope.flash_user_setup_model = res.data.msg;
            })


        }, function error(res) {
            if (res.status == 403) {
                window.location = '/';
            } else
                return $scope.flash_user_setup_model = res.data.msg;
        });
    };

    $scope.getUserDetails = function (id) {
        $location.path(('/users/details/' + id));
    };

    $scope.datePopup = {
        a: false,
        b: false,
        c: false,
        d: false,
        e: false
    };

    $scope.openDatePopup = function (id) {
        $scope.datePopup[id] = true;
        // $scope.popup1.opened = true;
    };
}]);
