/**
 * Created by oteng on 2/28/17.
 */

var HerbalTech = angular.module('HerbalTech', [
    //we add route dependency
    'ngRoute',
    'ui.bootstrap',
    //our controller which is an array of controllers
    'HerbalTechControllers'
]);


HerbalTech.factory('SharedUserDetails', function () {

    var _data = {};
    return {
        add: function (key, data) {
            _data[key] = data;
        },
        get: function (key) {
            return _data[key];
        }
    };

});


//now let set up the route for our app
HerbalTech.config([
    '$locationProvider', '$routeProvider',
    function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider
            .when('/dashboard', {
                templateUrl: '/templates/dashboard/index.html',
                controller: 'DashboardIndexController'
            })
            .when('/product', {
                templateUrl: '/templates/product/index.html',
                controller: 'ProductIndexController'
            })
            .when('/payment', {
                templateUrl: '/templates/payment/index.html'
            })
            .when('/sales', {
                templateUrl: '/templates/sales/index.html',
                controller: 'SalesIndexController'
            })
            .when('/users', {
                templateUrl: '/templates/users/index.html',
                controller: 'UserControllerIndex'
            })
            .when('/users/details/:userId', {
                templateUrl: '/templates/users/user_details.html',
                controller: 'UserDetailsController'
            })
            .when('/product/details/:productId', {
                templateUrl: '/templates/product/product_details.html',
                controller: 'ProductDetailsController'
            })
            .when('/sales/details/:salesId', {
                templateUrl: '/templates/sales/sales_details.html',
                controller: 'SalesDetailsController'
            })
            .when('/sales/direct_cash:salesId', {
                templateUrl: '/templates/sales/direct_cash_details.html',
                controller: 'SalesDetailsController'
            })
            .when('/report', {
                templateUrl: '/templates/report/index.html'
            })
            .when('/sales_app', {
                templateUrl: '/templates/sales/app.html',
                controller: 'SalesAppController'
            })
            .when('/product/batch/:id', {
                templateUrl: '/templates/product/batch_details.html',
                controller: 'ProductBatchController'
            })
            .when('/product/details_dis/:id', {
                templateUrl: '/templates/product/product_details_dis.html',
                controller: 'ProductDetailsDisController'
            })
            .otherwise({redirectTo: '/dashboard'});
    }
]);

var HerbalTechControllers = angular.module('HerbalTechControllers', ['chieffancypants.loadingBar',
    'ngAnimate']);

HerbalTechControllers.config(function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
});


var eventEmitter = function (data, $scope) {
    $scope.$emit('HT:layoutController:setActiveNavs', data);
};



modal_hide = function (modal) {
    if (modal) {
        $('#' + modal).hide();
        $('.modal-backdrop').hide();
    } else {
        $('#data_model').hide();
        $('.modal-backdrop').hide();
    }
};
