/**
 * Created by Eric.Badger on 3/15/2017.
 */
HerbalTechControllers.controller('ProductDetailsController', ['$scope', '$http', 'cfpLoadingBar', '$routeParams','$route', function ($scope, $http, cfpLoadingBar, $routeParams, $route) {

    // set the correct navs
    // eventEmitter({topNav: '/templates/users/top_nav.html', activeTopNav: '', activeSideBar: 'users'}, $scope);

    var ProductID = $routeParams.productId;
    // console.log(ProductID);
    cfpLoadingBar.start();
    $http.get(('/product/details/' + ProductID)).then(function ok(res) {
        $scope.product_details = res.data;
        $scope.distribute_list = res.data.distributors;
        console.log($scope.product_details);
        // console.log(ProductID);
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    $http.get('/product/distributor').then(function ok(res) {
        // console.log(res);
        $scope.distributors = res.data;
        // console.log($scope.distributors);
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    $scope.issue_product = function () {
        // console.log($scope.user_setup_model);
        //let us make sure all required fields are filled
        var keys2 = ['distributor', 'quantity'];
        for (var i = 0; i < keys2.length; i++) {
            if (!(keys2[i] in $scope.product_setup_model.distribute)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        //check username first
        cfpLoadingBar.start();
        $scope.product_setup_model.distribute.product_id = $scope.product_details.tbl_product_item.id;
        //very thing is fine push data to server
        $http.post("/product/issue_product", $scope.product_setup_model.distribute).then(function ok(res) {
            // console.log(res.data);
            modal_hide("distribute_model");
            $route.reload();
        }, function error(res) {
            if (res.status == 403) {
                window.location = "/";
            } else
                return $scope.flash_user_setup_model = res.data.msg;
        });
    };
}]);
