/**
 * Created by oteng on 3/13/17.
 */
HerbalTechControllers.controller('ProductIndexController', ['$scope', '$http', 'cfpLoadingBar', '$location', '$route', 'SharedUserDetails', function ($scope, $http, cfpLoadingBar, $location, $route, SharedUserDetails) {
    $scope.role_weight = SharedUserDetails.get('role_weight');
    // set the correct navs
    if ($scope.role_weight > 2)
        eventEmitter({topNav: '/templates/product/top_nav.html', activeTopNav: '', activeSideBar: 'product'}, $scope);
    else
        eventEmitter({topNav: '', activeTopNav: '', activeSideBar: 'product'}, $scope);
    // eventEmitter({activeSideBar: 'product'}, $scope);

    cfpLoadingBar.start();
    $http.get('/product').then(function ok(res) {
        console.log(res);
        $scope.product_items = res.data;
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    //get list of products
    $http.get('/product/list_of_products').then(function ok(res) {
        // console.log(res);
        $scope.products = res.data;
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    $http.get('/product/product_name').then(function ok(res) {
        // console.log(res);
        $scope.product_names = res.data;
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    $http.get('/product/batch_number').then(function ok(res) {
        $scope.batch_nums = res.data;
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    //model for user setup
    $scope.product_setup_model = {
        product_info: {},
        distribute: {},
        batch: {}
    };

    $scope.new_product = function () {
        // console.log($scope.user_setup_model);
        //let us make sure all required fields are filled
        var keys = ['name', 'description'];
        for (var i = 0; i < keys.length; i++) {
            if (!(keys[i] in $scope.product_setup_model.product_info)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        //check username first
        cfpLoadingBar.start();

        //very thing is fine push data to server

        $http.post("/product/create", $scope.product_setup_model.product_info).then(function ok(res) {
            // console.log(res);
            // location.reload();
            $route.reload();
        }, function error(res) {
            if (res.status == 403) {
                window.location = "/";
            } else
                return $scope.flash_user_setup_model = res.data.msg;
        });
        // }, function error(res) {
        //     if (res.status == 403) {
        //         window.location = '/';
        //     } else
        //         return $scope.flash_user_setup_model = res.data.msg;
        // });

    };

    $scope.create_product_item = function () {
        // console.log($scope.user_setup_model);
        //let us make sure all required fields are filled
        var keys = ['name', 'quantity', 'details', 'left', 'price'];
        for (var i = 0; i < keys.length; i++) {
            if (!(keys[i] in $scope.product_setup_model.product_info)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        //check username first
        cfpLoadingBar.start();
        //very thing is fine push data to server
        $scope.product_setup_model.product_info.batch_id = $scope.batch_display.id;
        console.log("product model", $scope.product_setup_model.product_info);
        $http.post("/product/create2", $scope.product_setup_model.product_info).then(function ok(res) {
            console.log(res);
            modal_hide("add_product_item_model");
            // location.reload();
            $route.reload();
        }, function error(res) {
            if (res.status == 403) {
                window.location = "/";
            } else
                return $scope.flash_user_setup_model = res.data.msg;
        });
    };

    $scope.create_batch = function () {
        console.log($scope.product_setup_model.batch);
        //let us make sure all required fields are filled
        var keys = ['batch_num', 'details'];
        for (var i = 0; i < keys.length; i++) {
            if (!(keys[i] in $scope.product_setup_model.batch)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        cfpLoadingBar.start();
        //very thing is fine push data to server
        $http.post("/product/create_batch", $scope.product_setup_model.batch).then(function ok(res) {
            // console.log(res);
            // $('#add_batch_model').modal('hide');
            modal_hide("add_batch_model");
            $scope.IsHidden = true;
            $scope.IsHidden2 = true;
            return $scope.batch_display = res.data
        }, function error(res) {
            if (res.status == 403) {
                window.location = "#!/product";
            } else
                return $scope.flash_user_setup_model = res.data.msg;
        });
    };

    $scope.getNumber = function () {
        $scope.product_setup_model.batch.batch_num = (Math.ceil(Math.random() * 999999));
    };

    $scope.select_batch = function () {
        $('#select_batch_model').modal('hide');
        $scope.IsHidden = true;
        $scope.IsHidden2 = false;
        var selected_batch_num = $scope.tmp_select_batch_num.split(',');
        // console.log($scope.product_setup_model.batch.batch_num);
        // var batch_id_num = $scope.product_setup_model.batch.batch_num.split(',');
        $scope.batch_display = {};
        $scope.batch_display.batch_num = selected_batch_num[1];
        $scope.batch_display.id = selected_batch_num[0];
    };

    $scope.getProductDetails = function (id) {
        if ($scope.role_weight <= 2) {
            $location.path(('/product/details_dis/' + id));
        } else
            $location.path(('/product/details/' + id));
    };

    $scope.getBatchDetails = function (id) {
        // console.log(id);
        $location.path(('/product/batch/' + id));
    }
}]);
