/**
 * Created by oteng on 3/27/17.
 */
HerbalTechControllers.controller('ProductBatchController', ['$scope', '$http', 'cfpLoadingBar','$location', '$routeParams', function ($scope, $http, cfpLoadingBar, $location, $routeParams) {

    var batch_id = $routeParams.id;
    // console.log("batch id in details", batch_id)
    //get the batch number
    cfpLoadingBar.start();
    $http.get('product/batch/'+batch_id).then(function ok(res) {
        // console.log(res.data);
        $scope.batches = res.data;
    }, function error(res) {
        if (res.status == 403) {
            window.location = "#!/product";
        } else
            return $scope.flash_user_setup_model = res.data.msg;
    });

    $scope.getProductDetails = function (id) {
        // console.log(id);
        $location.path(('/product/details/'+id));
    };

}]);
