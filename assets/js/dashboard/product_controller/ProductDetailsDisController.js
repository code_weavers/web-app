/**
 * Created by oteng on 3/27/17.
 */
HerbalTechControllers.controller('ProductDetailsDisController', ['$scope', '$http', 'cfpLoadingBar', '$routeParams','SharedUserDetails','$route', function ($scope, $http, cfpLoadingBar, $routeParams,SharedUserDetails, $route) {

    $scope.role_weight = SharedUserDetails.get('role_weight');

    var ProductID = $routeParams.id;
    // console.log(ProductID);
    cfpLoadingBar.start();
    $http.get(('/product/details/' + ProductID)).then(function ok(res) {
        $scope.product = res.data;
        // console.log(res.data)
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    //get the distributors users
    cfpLoadingBar.start();
    $http.get('/product/my_users/').then(function ok(res) {
        console.log(res);
        $scope.dis_lists = res.data;
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    //get the list of agents and sales persons that has been given the product
    $http.get('/product/dis_product_distribution/' + ProductID).then(function ok(res) {
        $scope.dis_summary = res.data;
        console.log(res.data)
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });


    $scope.confirm_receipt = function (id) {
        // console.log(id);
        $http.get('/product/confirm/'+id).then(function ok(res) {
           $route.reload();
        }, function error(res) {
            if (res.status == 403) {
                window.location = '/';
            }
        });
    };

    $scope.issue_product = function () {
        // console.log($scope.distribute);
        $scope.distribute.product = ProductID;
        $http.post('/product/dis_issue', $scope.distribute).then(function ok(res) {
            // console.log(res);
            // $scope.dis_lists = res.data;
            $route.reload();
            modal_hide('distribute_model');
        }, function error(res) {
            if (res.status == 403) {
                window.location = '/';
            }
        });
    }

}]);
