/**
 * Created by Eric.Badger on 3/25/2017.
 */
HerbalTechControllers.controller('SalesDetailsController', ['$scope', '$http', 'cfpLoadingBar', '$routeParams', function ($scope, $http, cfpLoadingBar, $routeParams) {

    // set the correct navs
    // eventEmitter({topNav: '/templates/users/top_nav.html', activeTopNav: '', activeSideBar: 'users'}, $scope);

    var SalesID = $routeParams.salesId;

    cfpLoadingBar.start();
    $http.get(('/sales/details/' + SalesID)).then(function ok(res) {
        console.log(res.data);
        $scope.details = res.data;
        // $scope.distribute_list = res.data.distributors;
        console.log($scope.details);
        // console.log(ProductID);
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    $http.get(('/sales/direct_cash_details/' + SalesID)).then(function ok(res) {
        console.log(res.data);
        $scope.direct_cash_details = res.data;
        // $scope.distribute_list = res.data.distributors;
        console.log($scope.details);
        // console.log(ProductID);
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });


    //model for user setup
    $scope.dashboard_setup_model = {
        serial: {}
    };


    $scope.new_serial = function () {
        // console.log($scope.user_setup_model);
        //let us make sure all required fields are filled
        var keys = ['serial'];
        for (var i = 0; i < keys.length; i++) {
            if (!(keys[i] in $scope.dashboard_setup_model.serial)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        //check username first
        cfpLoadingBar.start();
        //very thing is fine push data to server
        $scope.dashboard_setup_model.serial.salesId = $scope.details.id;
        $http.post("/sales/new_serial", $scope.dashboard_setup_model.serial).then(function ok(res) {
            console.log(res.data);
            $('#myModalSerialNo').modal('hide');
            location.reload();

        }, function error(res) {
            if (res.status == 403) {
                window.location = "/";
            } else
                return $scope.flash_user_setup_model = res.data.msg;
        });

    };

    // $http.get(('/sales/cash_details/' + SalesID)).then(function ok(res) {
    //     console.log(res.data);
    //     $scope.cash_details = res.data;
    //     // $scope.distribute_list = res.data.distributors;
    //     console.log($scope.sales_details);
    //     // console.log(ProductID);
    // }, function error(res) {
    //     if (res.status == 403) {
    //         window.location = '/';
    //     }
    // });

    // $http.get('/product/distributor').then(function ok(res) {
    //     // console.log(res);
    //     $scope.distributors = res.data;
    //     console.log($scope.distributors);
    // }, function error(res) {
    //     if (res.status == 403) {
    //         window.location = '/';
    //     }
    // });
}]);