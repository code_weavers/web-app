/**
 * Created by Eric.Badger on 3/22/2017.
 */
HerbalTechControllers.controller('SalesIndexController', ['$scope', '$http', 'cfpLoadingBar', '$location',function ($scope, $http, cfpLoadingBar, $location) {
    // set the correct navs
    eventEmitter({topNav: '/templates/sales/top_nav.html', activeTopNav: '', activeSideBar: 'sales'}, $scope);
    // eventEmitter({activeSideBar: 'product'}, $scope);
    cfpLoadingBar.start();

    $http.get('/sales/direct_cash').then(function ok(res) {
        // console.log(res);
        $scope.direct_cashes = res.data;
        console.log($scope.direct_cashes);
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    $http.get('/sales/deduction').then(function ok(res) {
        // console.log(res);
        $scope.deductions = res.data;
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    $http.get('/sales/customer').then(function ok(res) {
        // console.log(res);
        $scope.customers = res.data;
    }, function error(res) {
        if (res.status == 403) {
            window.location = '/';
        }
    });

    //model for user setup
    $scope.sales_setup_model = {
        deduction: {}
    };

    $scope.new_deduction = function () {
        // console.log($scope.user_setup_model);
        //let us make sure all required fields are filled
        var keys = ['amount'];
        for (var i = 0; i < keys.length; i++) {
            if (!(keys[i] in $scope.sales_setup_model.deduction)) {
                return $scope.flash_user_setup_model = 'All fields with * are required'
            }
        }

        //check username first
        cfpLoadingBar.start();
        //very thing is fine push data to server
        $http.post("/sales/new_deduction", $scope.sales_setup_model.deduction).then(function ok(res) {
            console.log(res);
            $('#myModalDeduction').modal('hide');
            location.reload();
        }, function error(res) {
            if (res.status == 403) {
                window.location = "/";
            } else
                return $scope.flash_user_setup_model = res.data.msg;
        });
        // }, function error(res) {
        //     if (res.status == 403) {
        //         window.location = '/';
        //     } else
        //         return $scope.flash_user_setup_model = res.data.msg;
        // });

    };
    $scope.getSalesDetails = function (id) {
        console.log(id);
        $location.path(('/sales/details/'+id));
    };

    $scope.getDirectCashDetails = function (id) {
        console.log(id);
        $location.path(('/sales/direct_cash'+id));
    }
}]);