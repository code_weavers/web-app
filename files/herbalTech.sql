-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2017 at 11:46 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `herbalTech`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `tbl_user` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `tbl_role` int(11) DEFAULT NULL,
  `tbl_password` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`tbl_user`, `username`, `tbl_role`, `tbl_password`, `id`, `createdAt`, `updatedAt`) VALUES
(1, 'su', 1, 1, 1, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(2, 'admin', 2, 2, 2, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(3, 'dis1', 3, 3, 3, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(4, 'dis2', 3, 4, 4, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(5, 'agent1', 4, 5, 5, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(6, 'agent2', 4, 6, 6, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(7, 'agent3', 4, 7, 7, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(8, 'agent4', 4, 8, 8, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(9, 'sale1', 5, 9, 9, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(10, 'sale2', 5, 10, 10, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(11, 'sale3', 5, 11, 11, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(12, 'sale4', 5, 12, 12, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(13, 'sale5', 5, 13, 13, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(14, 'sale6', 5, 14, 14, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(15, 'sale7', 5, 15, 15, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(16, 'sale8', 5, 16, 16, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(17, 'oteng', 2, 17, 17, '2017-03-10 23:13:02', '2017-03-10 23:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_password`
--

CREATE TABLE `tbl_password` (
  `pass` varchar(255) DEFAULT NULL,
  `reset` tinyint(1) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_password`
--

INSERT INTO `tbl_password` (`pass`, `reset`, `id`, `createdAt`, `updatedAt`) VALUES
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 0, 1, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 2, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 3, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 4, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 5, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 6, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 7, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 8, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 9, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 10, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 11, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 12, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 13, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 14, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 15, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO', 1, 16, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('$2a$10$n4YRCh3zHz9eLr8cUcFC/.8RYCSINGavKWIuNaQ68yEofAv2to9x2', 1, 17, '2017-03-10 23:13:02', '2017-03-10 23:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_phone`
--

CREATE TABLE `tbl_phone` (
  `number` varchar(255) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `expire` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `tbl_user` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_phone`
--

INSERT INTO `tbl_phone` (`number`, `verified`, `code`, `expire`, `rank`, `tbl_user`, `id`, `createdAt`, `updatedAt`) VALUES
('+233506592852', 1, NULL, NULL, 1, 1, 1, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 2, 2, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 3, 3, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 4, 4, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 5, 5, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 6, 6, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 7, 7, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 8, 8, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 9, 9, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 10, 10, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 11, 11, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 12, 12, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 13, 13, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 14, 14, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 15, 15, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592852', 0, NULL, NULL, 1, 16, 16, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('+233506592851', NULL, NULL, NULL, NULL, 17, 17, '2017-03-10 23:13:02', '2017-03-10 23:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE `tbl_role` (
  `role` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`role`, `weight`, `id`, `createdAt`, `updatedAt`) VALUES
('su', 4, 1, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('admin', 3, 2, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('dis', 2, 3, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('agent', 1, 4, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('sales', 0, 5, '2017-03-01 14:00:03', '2017-03-01 14:00:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `f_name` varchar(255) DEFAULT NULL,
  `l_name` varchar(255) DEFAULT NULL,
  `m_name` varchar(255) DEFAULT NULL,
  `pix` longtext,
  `dob` date DEFAULT NULL,
  `hometown` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `hobbies` varchar(255) DEFAULT NULL,
  `profession` varchar(255) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`f_name`, `l_name`, `m_name`, `pix`, `dob`, `hometown`, `nationality`, `hobbies`, `profession`, `id`, `createdAt`, `updatedAt`) VALUES
('Kwame', 'Menu', NULL, NULL, '1982-02-14', 'Makola', 'Ghananian', 'Reading', 'Teacher', 1, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Maku', 'Kuku', NULL, NULL, '1992-04-04', 'Kumasi', 'Ghananian', 'Sports', 'Trader', 2, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Kwaku', 'Huma', NULL, NULL, '1989-12-14', 'Accra', 'Ghananian', 'Reading', 'Software developer', 3, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Gilbet', 'Kuma', NULL, NULL, '1990-05-10', 'Accra', 'Ghananian', 'Sports', 'Trader', 4, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Kwaku', 'Mensa', NULL, NULL, '1970-01-01', 'Ho', 'Ghananian', 'Sports', 'Labour', 5, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Ajoa', 'Kumi', NULL, NULL, '1989-09-23', 'Westland', 'Ghananian', 'Sports', 'Farmer', 6, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Kwesi', 'Azuma', NULL, NULL, '1992-07-18', 'Agomeda', 'Ghananian', 'Fishing', 'Farmer', 7, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Sefam', 'Yusia', NULL, NULL, '1986-12-24', 'Madina', 'Ghananian', 'Video Games', 'Farmer', 8, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Marta', 'Ansa', NULL, NULL, '1991-08-10', 'Ashiaman', 'Ghananian', 'Video Games', 'Doctor', 9, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Martin', 'Saky', NULL, NULL, '1985-03-01', 'Takurade', 'Ghananian', 'Reading', 'Fisherman', 10, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Saky', 'Anan', NULL, NULL, '1991-05-07', 'Oyibi', 'Ghananian', 'Video Games', 'Driver', 11, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Kukua', 'Kofi', NULL, NULL, '1982-02-14', 'Westland', 'Ghananian', 'Reading', 'Driver', 12, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Jessica', 'Jonson', NULL, NULL, '1982-02-14', 'Airport', 'Ghananian', 'Video Games', 'Printer', 13, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Josphen', 'Awakwi', NULL, NULL, '1982-02-14', 'Sokakopki', 'Ghananian', 'Reading', 'Teacher', 14, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Eric', 'Twiku', NULL, NULL, '1982-02-14', 'Suraduo', 'Ghananian', 'Sports', 'Painter', 15, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Joy', 'Atiku', NULL, NULL, '1982-02-14', 'Usila', 'Ghananian', 'Sports', 'Taki', 16, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Kwaku', 'Oteng', NULL, NULL, '1986-01-29', 'Akrosu', 'Ghanaian', 'Reading', 'Teacher', 17, '2017-03-10 23:13:02', '2017-03-10 23:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_contact`
--

CREATE TABLE `tbl_user_contact` (
  `address` varchar(255) DEFAULT NULL,
  `direction` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tbl_user` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_contact`
--

INSERT INTO `tbl_user_contact` (`address`, `direction`, `email`, `tbl_user`, `id`, `createdAt`, `updatedAt`) VALUES
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 1, 1, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 2, 2, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 3, 3, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 4, 4, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 5, 5, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 6, 6, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 7, 7, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 8, 8, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 9, 9, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 10, 10, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 11, 11, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 12, 12, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 13, 13, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 14, 14, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 15, 15, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Makola Block b5', 'Westland Police Post', 'example@gmail.com', 16, 16, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(NULL, NULL, NULL, 17, 17, '2017-03-10 23:13:02', '2017-03-10 23:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_education`
--

CREATE TABLE `tbl_user_education` (
  `type_of_edu` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `trainer` varchar(255) DEFAULT NULL,
  `trainer_contact` varchar(255) DEFAULT NULL,
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `tbl_user` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_education`
--

INSERT INTO `tbl_user_education` (`type_of_edu`, `school`, `trainer`, `trainer_contact`, `from`, `to`, `tbl_user`, `id`, `createdAt`, `updatedAt`) VALUES
('SSCE', 'Sukuar SHS', NULL, NULL, NULL, NULL, 1, 1, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Aprenteship', NULL, 'Master Kuman Duku', '+233209450219', NULL, NULL, 2, 2, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('SSCE', 'Sukuar SHS', NULL, NULL, NULL, NULL, 3, 3, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Aprenteship', NULL, 'Master Kuman Duku', '+233209450219', NULL, NULL, 4, 4, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('SSCE', 'Sukuar SHS', NULL, NULL, NULL, NULL, 5, 5, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Aprenteship', NULL, 'Master Kuman Duku', '+233209450219', NULL, NULL, 6, 6, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('SSCE', 'Sukuar SHS', NULL, NULL, NULL, NULL, 7, 7, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Aprenteship', NULL, 'Master Kuman Duku', '+233209450219', NULL, NULL, 8, 8, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('SSCE', 'Sukuar SHS', NULL, NULL, NULL, NULL, 9, 9, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Aprenteship', NULL, 'Master Kuman Duku', '+233209450219', NULL, NULL, 10, 10, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('SSCE', 'Sukuar SHS', NULL, NULL, NULL, NULL, 11, 11, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Aprenteship', NULL, 'Master Kuman Duku', '+233209450219', NULL, NULL, 12, 12, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('SSCE', 'Sukuar SHS', NULL, NULL, NULL, NULL, 13, 13, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Aprenteship', NULL, 'Master Kuman Duku', '+233209450219', NULL, NULL, 14, 14, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('SSCE', 'Sukuar SHS', NULL, NULL, NULL, NULL, 15, 15, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('Aprenteship', NULL, 'Master Kuman Duku', '+233209450219', NULL, NULL, 16, 16, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(NULL, NULL, NULL, NULL, NULL, NULL, 17, 17, '2017-03-10 23:13:02', '2017-03-10 23:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_employment`
--

CREATE TABLE `tbl_user_employment` (
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `duties` varchar(255) DEFAULT NULL,
  `reason_for_leaving` varchar(255) DEFAULT NULL,
  `referee` varchar(255) DEFAULT NULL,
  `tbl_user` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_employment`
--

INSERT INTO `tbl_user_employment` (`from`, `to`, `name`, `position`, `duties`, `reason_for_leaving`, `referee_name`, `tbl_user`, `id`, `createdAt`, `updatedAt`) VALUES
('1975-03-01', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 1, 1, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 2, 2, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 3, 3, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 4, 4, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 5, 5, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 6, 6, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 7, 7, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 8, 8, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 9, 9, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 10, 10, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 11, 11, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 12, 12, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 13, 13, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 14, 14, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 15, 15, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
('1975', '2006', 'Mina LTD', 'Marketer', 'Selling', NULL, 'Mamuna +23350989876', 16, 16, '2017-03-01 14:00:03', '2017-03-01 14:00:03'),
(NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17, 17, '2017-03-10 23:13:02', '2017-03-10 23:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_family`
--

CREATE TABLE `tbl_user_family` (
  `fa_name` varchar(255) DEFAULT NULL,
  `fa_occupation` varchar(255) DEFAULT NULL,
  `fa_nationality` varchar(255) DEFAULT NULL,
  `mo_name` varchar(255) DEFAULT NULL,
  `mo_occupation` varchar(255) DEFAULT NULL,
  `mo_nationality` varchar(255) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `num_children` varchar(255) DEFAULT NULL,
  `next_of_kin` varchar(255) DEFAULT NULL,
  `nok_contact` varchar(255) DEFAULT NULL,
  `tbl_user` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_family`
--

INSERT INTO `tbl_user_family` (`fa_name`, `fa_occupation`, `fa_nationality`, `mo_name`, `mo_occupation`, `mo_nationality`, `marital_status`, `num_children`, `next_of_kin`, `nok_contact`, `tbl_user`, `id`, `createdAt`, `updatedAt`) VALUES
(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17, 1, '2017-03-10 23:13:02', '2017-03-10 23:13:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_user` (`tbl_user`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tbl_password`
--
ALTER TABLE `tbl_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_phone`
--
ALTER TABLE `tbl_phone`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_user` (`tbl_user`);

--
-- Indexes for table `tbl_role`
--
ALTER TABLE `tbl_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_contact`
--
ALTER TABLE `tbl_user_contact`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_user` (`tbl_user`);

--
-- Indexes for table `tbl_user_education`
--
ALTER TABLE `tbl_user_education`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_user` (`tbl_user`);

--
-- Indexes for table `tbl_user_employment`
--
ALTER TABLE `tbl_user_employment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_user` (`tbl_user`);

--
-- Indexes for table `tbl_user_family`
--
ALTER TABLE `tbl_user_family`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_user` (`tbl_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_password`
--
ALTER TABLE `tbl_password`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_phone`
--
ALTER TABLE `tbl_phone`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_role`
--
ALTER TABLE `tbl_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_user_contact`
--
ALTER TABLE `tbl_user_contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_user_education`
--
ALTER TABLE `tbl_user_education`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_user_employment`
--
ALTER TABLE `tbl_user_employment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_user_family`
--
ALTER TABLE `tbl_user_family`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
