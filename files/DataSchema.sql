CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Password` (
  `id` INT NOT NULL,
  `pass` VARCHAR(255) NULL,
  `reset` TINYINT(1) NULL,
  PRIMARY KEY (`id`))

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Login` (
  `id` INT NOT NULL,
  `username` VARCHAR(45) NULL,
  `tbl_password` INT NOT NULL,
  `tbl_role` INT NOT NULL,
  PRIMARY KEY (`id`, `tbl_password`, `tbl_role`),
  INDEX `fk_Tbl_Login_Tbl_Password1_idx` (`tbl_password` ASC),
  INDEX `fk_Tbl_Login_Tbl_Role1_idx` (`tbl_role` ASC),
  CONSTRAINT `fk_Tbl_Login_Tbl_Password1`
    FOREIGN KEY (`tbl_password`)
    REFERENCES `mydb`.`Tbl_Password` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Login_Tbl_Role1`
    FOREIGN KEY (`tbl_role`)
    REFERENCES `mydb`.`Tbl_Role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Phone` (
  `id` INT NOT NULL,
  `number` VARCHAR(45) NULL,
  `verified` TINYINT(1) NULL,
  `code` VARCHAR(10) NULL,
  `expire` DATETIME NULL,
  `rank` INT NULL,
  `tbl_user` INT NOT NULL,
  PRIMARY KEY (`id`, `tbl_user`),
  INDEX `fk_Tbl_Phone_Tbl_User1_idx` (`tbl_user` ASC),
  CONSTRAINT `fk_Tbl_Phone_Tbl_User1`
    FOREIGN KEY (`tbl_user`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Role` (
  `id` INT NOT NULL,
  `role` VARCHAR(10) NULL,
  `weight` INT NULL,
  PRIMARY KEY (`id`))

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Product_Item` (
  `id` INT NOT NULL,
  `quantity` INT NULL,
  `details` VARCHAR(255) NULL,
  `left` INT NULL,
  `Tbl_Product` INT NOT NULL,
  PRIMARY KEY (`id`, `Tbl_Product`),
  INDEX `fk_Tbl_Product_Item_Tbl_Product1_idx` (`Tbl_Product` ASC),
  CONSTRAINT `fk_Tbl_Product_Item_Tbl_Product1`
    FOREIGN KEY (`Tbl_Product`)
    REFERENCES `mydb`.`Tbl_Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Product_Distributor` (
  `id` INT NOT NULL,
  `quantity` INT NULL,
  `left` VARCHAR(45) NULL,
  `tbl_user` INT NOT NULL,
  `tbl_product_batch` INT NOT NULL,
  `confirm` TINYINT(1) NULL,
  PRIMARY KEY (`id`, `tbl_user`, `tbl_product_batch`),
  INDEX `fk_Tbl_Product_User_Tbl_User1_idx` (`tbl_user` ASC),
  INDEX `fk_Tbl_Product_User_Tbl_Product_Batch1_idx` (`tbl_product_batch` ASC),
  CONSTRAINT `fk_Tbl_Product_User_Tbl_User1`
    FOREIGN KEY (`tbl_user`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Product_User_Tbl_Product_Batch1`
    FOREIGN KEY (`tbl_product_batch`)
    REFERENCES `mydb`.`Tbl_Product_Batch` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Product_Agent` (
  `id` INT NOT NULL,
  `quantity` INT NULL,
  `left` VARCHAR(45) NULL,
  `tbl_user` INT NOT NULL,
  `tbl_product_batch` INT NOT NULL,
  `confirm` TINYINT(1) NULL,
  PRIMARY KEY (`id`, `tbl_user`, `tbl_product_batch`),
  INDEX `fk_Tbl_Product_User_Tbl_User1_idx` (`tbl_user` ASC),
  INDEX `fk_Tbl_Product_User_Tbl_Product_Batch1_idx` (`tbl_product_batch` ASC),
  CONSTRAINT `fk_Tbl_Product_User_Tbl_User10`
    FOREIGN KEY (`tbl_user`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Product_User_Tbl_Product_Batch10`
    FOREIGN KEY (`tbl_product_batch`)
    REFERENCES `mydb`.`Tbl_Product_Batch` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Product_Sales_Person` (
  `id` INT NOT NULL,
  `quantity` INT NULL,
  `left` VARCHAR(45) NULL,
  `tbl_user` INT NOT NULL,
  `tbl_product_batch` INT NOT NULL,
  `confirm` TINYINT(1) NULL,
  PRIMARY KEY (`id`, `tbl_user`, `tbl_product_batch`),
  INDEX `fk_Tbl_Product_User_Tbl_User1_idx` (`tbl_user` ASC),
  INDEX `fk_Tbl_Product_User_Tbl_Product_Batch1_idx` (`tbl_product_batch` ASC),
  CONSTRAINT `fk_Tbl_Product_User_Tbl_User11`
    FOREIGN KEY (`tbl_user`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Product_User_Tbl_Product_Batch11`
    FOREIGN KEY (`tbl_product_batch`)
    REFERENCES `mydb`.`Tbl_Product_Batch` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Product_Batch` (
  `id` INT NOT NULL,
  `tbl_product_item` INT NOT NULL,
  `tbl_batch` INT NOT NULL,
  `tbl_user` INT NOT NULL,
  PRIMARY KEY (`id`, `tbl_product_item`, `tbl_batch`, `tbl_user`),
  INDEX `fk_Tbl_Product_Batch_Tbl_Product1_idx` (`tbl_product_item` ASC),
  INDEX `fk_Tbl_Product_Batch_Tbl_Batch1_idx` (`tbl_batch` ASC),
  INDEX `fk_Tbl_Product_Batch_Tbl_User1_idx` (`tbl_user` ASC),
  CONSTRAINT `fk_Tbl_Product_Batch_Tbl_Product1`
    FOREIGN KEY (`tbl_product_item`)
    REFERENCES `mydb`.`Tbl_Product_Item` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Product_Batch_Tbl_Batch1`
    FOREIGN KEY (`tbl_batch`)
    REFERENCES `mydb`.`Tbl_Batch` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Product_Batch_Tbl_User1`
    FOREIGN KEY (`tbl_user`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Batch` (
  `id` INT NOT NULL,
  `batch_num` VARCHAR(45) NULL,
  `details` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Product` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))


CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Agent_Sales` (
  `id` INT NOT NULL,
  `agent` INT NOT NULL,
  `sales_person` INT NOT NULL,
  PRIMARY KEY (`id`, `agent`, `sales_person`),
  INDEX `fk_Tbl_Agent_Sales_Tbl_User1_idx` (`agent` ASC),
  INDEX `fk_Tbl_Agent_Sales_Tbl_User2_idx` (`sales_person` ASC),
  CONSTRAINT `fk_Tbl_Agent_Sales_Tbl_User1`
    FOREIGN KEY (`agent`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Agent_Sales_Tbl_User2`
    FOREIGN KEY (`sales_person`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_Distributor_Sales` (
  `id` INT NOT NULL,
  `distributor` INT NOT NULL,
  `sales_person` INT NOT NULL,
  PRIMARY KEY (`id`, `distributor`, `sales_person`),
  INDEX `fk_Tbl_Distributor_Sales_Tbl_User1_idx` (`distributor` ASC),
  INDEX `fk_Tbl_Distributor_Sales_Tbl_User2_idx` (`sales_person` ASC),
  CONSTRAINT `fk_Tbl_Distributor_Sales_Tbl_User1`
    FOREIGN KEY (`distributor`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tbl_Distributor_Sales_Tbl_User2`
    FOREIGN KEY (`sales_person`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE TABLE IF NOT EXISTS `mydb`.`Tble_Distributor_Agent` (
  `id` INT NOT NULL,
  `distributor` INT NOT NULL,
  `agent` INT NOT NULL,
  PRIMARY KEY (`id`, `distributor`, `agent`),
  INDEX `fk_Tble_Distributor_Agent_Tbl_User1_idx` (`distributor` ASC),
  INDEX `fk_Tble_Distributor_Agent_Tbl_User2_idx` (`agent` ASC),
  CONSTRAINT `fk_Tble_Distributor_Agent_Tbl_User1`
    FOREIGN KEY (`distributor`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tble_Distributor_Agent_Tbl_User2`
    FOREIGN KEY (`agent`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_User_Employment` (
  `id` INT NOT NULL,
  `from` DATE NULL,
  `to` DATE NULL,
  `name` VARCHAR(45) NULL,
  `position` VARCHAR(45) NULL,
  `duties` VARCHAR(255) NULL,
  `reason_for_leaving` VARCHAR(255) NULL,
  `referee_name` VARCHAR(45) NULL,
  `referee_contact` VARCHAR(45) NULL,
  `tbl_user` INT NOT NULL,
  PRIMARY KEY (`id`, `tbl_user`),
  INDEX `fk_Tbl_User_Employment_Tbl_User1_idx` (`tbl_user` ASC),
  CONSTRAINT `fk_Tbl_User_Employment_Tbl_User1`
    FOREIGN KEY (`tbl_user`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)


CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_User_Contact` (
  `id` INT NOT NULL,
  `address` VARCHAR(255) NULL,
  `direction` VARCHAR(255) NULL,
  `email` VARCHAR(45) NULL,
  `tbl_user` INT NOT NULL,
  PRIMARY KEY (`id`, `tbl_user`),
  INDEX `fk_Tbl_User_Contact_Tbl_User1_idx` (`tbl_user` ASC),
  CONSTRAINT `fk_Tbl_User_Contact_Tbl_User1`
    FOREIGN KEY (`tbl_user`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_User` (
  `id` INT NOT NULL,
  `f_name` VARCHAR(45) NULL,
  `l_name` VARCHAR(45) NULL,
  `m_name` VARCHAR(45) NULL,
  `pix` VARCHAR(255) NULL,
  `dob` DATE NULL,
  `hometown` VARCHAR(45) NULL,
  `nationality` VARCHAR(45) NULL,
  `hobbies` VARCHAR(255) NULL,
  `profession` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))


CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_User_Education` (
  `id` INT NOT NULL,
  `type_of_edu` VARCHAR(45) NULL,
  `school` VARCHAR(45) NULL,
  `trainer` VARCHAR(45) NULL,
  `trainer_contact` VARCHAR(45) NULL,
  `from` DATE NULL,
  `to` DATE NULL,
  `tbl_user` INT NOT NULL,
  PRIMARY KEY (`id`, `tbl_user`),
  INDEX `fk_Tbl_User_Education_Tbl_User1_idx` (`tbl_user` ASC),
  CONSTRAINT `fk_Tbl_User_Education_Tbl_User1`
    FOREIGN KEY (`tbl_user`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

CREATE TABLE IF NOT EXISTS `mydb`.`Tbl_User_Family` (
  `id` INT NOT NULL,
  `fa_name` VARCHAR(45) NULL,
  `fa_occupation` VARCHAR(45) NULL,
  `fa_nationality` VARCHAR(45) NULL,
  `mo_name` VARCHAR(45) NULL,
  `mo_occupation` VARCHAR(45) NULL,
  `mo_nationality` VARCHAR(45) NULL,
  `marital_status` VARCHAR(45) NULL,
  `num_children` INT NULL,
  `next_of_kin` VARCHAR(45) NULL,
  `nok_contact` VARCHAR(45) NULL,
  `tbl_user` INT NOT NULL,
  PRIMARY KEY (`id`, `tbl_user`),
  INDEX `fk_Tbl_User_Family_Tbl_User1_idx` (`tbl_user` ASC),
  CONSTRAINT `fk_Tbl_User_Family_Tbl_User1`
    FOREIGN KEY (`tbl_user`)
    REFERENCES `mydb`.`Tbl_User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

