/**
 * Created by oteng on 2/15/17.
 */

module.exports = [

    {
        id: 1,
        number: '+233506592851',
        verified: false,
        code: null,
        expire: null,
        rank: 'primary',
        tbl_user: 1
    },
    {
        id: 2,
        number: '0544930408',
        verified: false,
        code: null,
        expire: null,
        rank: 'primary',
        tbl_user: 2
    }
];
