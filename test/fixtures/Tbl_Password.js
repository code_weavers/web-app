/**
 * Created by oteng on 2/10/17.
 */


module.exports = [

    {
        id: 1,
        //pass: '$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO',
        pass: '123456',
        reset: false
    },
    {
        id: 2,
        //pass: '$2a$10$b5/KztUSR3AhQ09kgPIcB.Q3Hm7gApMenztqQPAfC2iVrprqS7nhO',
        pass: '123456',
        reset: true
    }
];

