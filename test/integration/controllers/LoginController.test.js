/**
 * Created by oteng on 5/29/16.
 */
var request = require('supertest');
var Barrels = require('barrels');
var should = require('should');
describe('Test Fixture for LoginController', function () {
    var barrels = new Barrels();
    describe('#login()', function () {
        it('should redirect to / if request is get', function (done) {
            request(sails.hooks.http.app)
                .get('/login/login')
                .expect('location', '/', done);

        });
        it('should redirect to login page if login error', function (done) {
            request(sails.hooks.http.app)
                .post('/login/login')
                .send({username: 'test', pass: ''})
                .expect('location', '/', done);

        });
    });

    describe('Test Login using data', function () {
        before(function (done) {
            barrels.populate(['tbl_password', 'tbl_role', 'tbl_user', 'tbl_login', 'tbl_phone'], function (err) {
                if (err) {
                    return done(err);
                }
                done();
            })
        });

        it('log you in', function (done) {
            request(sails.hooks.http.app)
                .post('/login/login')
                .send({username: 'test', pass: '123456'})
                .expect('location', '/dashboard', done);
        });

        it('it should direct you to the login page', function (done) {
            request(sails.hooks.http.app)
                .post('/login/login')
                .send({username: 'test', pass: '11'})
                .expect('location', '/', done);

        });

        it('should redirect to / since we are not log in', function (done) {
            request(sails.hooks.http.app)
                .get('/login/logout')
                .expect('location', '/', done)
        });

        it('should redirect to /login/reset since reset is true', function (done) {
            request(sails.hooks.http.app)
                .post('/login/login')
                .send({username: 'resetUser', pass: '123456'})
                .expect('location', '/login/reset', done)
        });

        it('should reset users password', function (done) {
            //request redirect has already been sent by previous test
            var cookie;
            request(sails.hooks.http.app)
                .post('/login/login')
                .send({username: 'resetUser', pass: '123456'})
                .end(function (e, req) {
                    cookie = req.headers['set-cookie'];
                    request(sails.hooks.http.app)
                        .post('/login/reset')
                        .set('Cookie', cookie)
                        .send({new_pass: '654321'})
                        .expect('location', '/dashboard', done);
                });
        });

        it('should login using the new password since password has been reset', function (done) {
            request(sails.hooks.http.app)
                .post('/login/login')
                .send({username: 'resetUser', pass: '654321'})
                .expect('location', '/dashboard', done)
        });


    });

    describe('Test Policies on Login:', function () {
        before(function (done) {
            barrels.populate(['tbl_password', 'tbl_role', 'tbl_user', 'tbl_login', 'tbl_phone'], function (err) {
                if (err) {
                    return done(err);
                }
                done();
            })
        });

        describe('When logged in:', function () {
            var cookie;
            before(function (done) {
                request(sails.hooks.http.app)
                    .post('/login/login')
                    .send({username: 'test', pass: '123456'})
                    .end(function (e, req) {
                        cookie = req.headers['set-cookie'];
                        done();
                    })
            });

            it("should redirect to dashboard", function (done) {
                request(sails.hooks.http.app)
                    .post('/login')
                    .set('Cookie', cookie)
                    .expect('location', '/dashboard', done)
            });

            it("should redirect to dashboard", function (done) {
                request(sails.hooks.http.app)
                    .post('/login/reset')
                    .set('Cookie', cookie)
                    .expect('location', '/dashboard', done)
            });

            it("should redirect to dashboard", function (done) {
                request(sails.hooks.http.app)
                    .post('/login/login')
                    .set('Cookie', cookie)
                    .expect('location', '/dashboard', done)
            });

            it('should generate code and set phone db', function (done) {
                request(sails.hooks.http.app)
                    .post('/login/send_verification_code')
                    .set('Cookie', cookie)
                    .send({phone_id: 1})
                    .end(function (err, res) {
                        Tbl_Phone.findOne({id: 1}, function (e, r) {
                            should(r.code).not.equal(null);
                            should(r.code).not.equal(null);
                            done();
                        });
                    })
            });

            it('should return the verify view', function (done) {
                request(sails.hooks.http.app)
                    .get('/login/verify_number')
                    .set('Cookie', cookie)
                    .expect(200, {view: 'phone_verify', "num": []}, done);
            });

            // it('should return error if the code and id is not given', function (done) {
            //     request(sails.hooks.http.app)
            //         .post('/login/verify_number')
            //         .set('Cookie', cookie)
            //         .expect(200, {result: true, error: 'flash', mess: 'code required'}, done);
            // });

            it('should tell me code dont much', function (done) {
                request(sails.hooks.http.app)
                    .post('/login/verify_number')
                    .set('Cookie', cookie)
                    .send({code: '123456', phone_id: 1})
                    .expect(200, {result: true, error: "mach", mess: "Code don't much"}, done);
            });

            it("should redirect you to very phone number", function (done) {
                request(sails.hooks.http.app)
                    .post('/login/logout')
                    .set('Cookie', cookie)
                    .expect('location', '/login/verify_number', done)
            });

        })

    })

});

