/**
 * Created by oteng on 5/29/16.
 *
 * This file lift's the sails server before the testing is done
 */
var sails = require('sails');
var Barrels = require('barrels');

before(function (done) {

    // Increase the Mocha timeout so that Sails has enough time to lift.
    this.timeout(5000);

    sails.lift({
        // configuration for testing purposes
        port: 6080,
        models: {
            connection: 'test'
        }

    }, function (err, server) {
        // here you can load fixtures, etc.
        done(err, sails);
    });
});

after(function (done) {
    // here you can clear fixtures, etc.
    sails.lower(done);
});
