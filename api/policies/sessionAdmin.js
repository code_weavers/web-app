/**
 * Created by oteng on 2/28/17.
 */
module.exports = function (req, res, next) {

    // User is allowed, proceed to the next policy,
    // or if this is the last policy, the controller
    if (req.session.role > 2) {
            return next();
    }

    // User is not allowed
    // redirect to the login page
    res.status('403');
    var _msg = "You don't have permission to access this route";
    return  res.json({msg: _msg});
};
