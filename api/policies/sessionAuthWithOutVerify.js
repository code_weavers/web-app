/**
 * Created by oteng on 3/29/17.
 */
module.exports = function (req, res, next) {

    // User is allowed, proceed to the next policy,
    // or if this is the last policy, the controller
    if (req.session.auth) {
        return next();
    }

    // User is not allowed
    // redirect to the login page
    return res.redirect('/');
};
