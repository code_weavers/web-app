/**
 * Created by oteng on 2/12/17.
 */
/**
 * notLogin
 *
 * @module      :: Policy
 * @description :: Simply policies to make sure that only those who are not logged
 *                  in can access a particular action
 *
 */
module.exports = function(req, res, next) {

    // User is allowed, proceed to the next policy,
    // or if this is the last policy, the controller
    if (req.session.auth) {
        return res.redirect('/dashboard');
    }

    next();
};
