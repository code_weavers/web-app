/**
 * Created by oteng on 8/3/16.
 */

var http = require('http');
module.exports = {

    bashUrl: 'http://121.241.242.114:8080/bulksms/bulksms?username=gbs-bitsweaver&password=bit@2000&type=0&dlr=1&',
    // prepareRequest: function (url) {
    //     /**
    //      * this function takes a string and returns the string as
    //      * URL-UTF-8
    //      * @params: (dic) contains the whole string feed to the http API
    //      */
    //     return encodeURI(url);
    // },

    countSMS: function (megs) {
        /**
         * This function is for counting the number of SMS that will be needed to send
         * the message.
         * @params: options - a dic of objects of which one is the mess
         * @return: (int) the number of SMS need to send that message
         */

        var smsMegsLen = 153;
        if(megs.length <= smsMegsLen)
            return 1;

        return 1 + this.countSMS(megs.slice(smsMegsLen));
    },

    sendMessage: function (url, cb) {
        return http.get(url, cb)
    },

    queue: function (options) {
        /**
         * If there are multiple users to send the message to this function is responsible for queuing theses
         * jobs and executing it.
         */
    },

    // prepareNum: function (arr) {
    //     var nums = '';
    //     if(arr.length == 0)
    //         return null;
    //     for(var i = 0; i < arr.length; i++ ){
    //         var num = arr[i];
    //         //check if number has the country code and append it assume Ghana
    //         if(num.length >! 10){
    //             //take out the first character and replace it with +233;
    //             nums += ('%2B233'+num.slice(1)+',');
    //         }
    //     }
    //     return nums.slice(0,(nums.length - 1));
    // },

    verifySourceLength: function (source) {
      return source.length <= 11;
    },

    send: function (opt, processReport) {
        /**
         * @params: opt
         * @type: dict
         * @description: this is the main method for sending sms it uses all the components of
         * this class
         */
        // var destination = this.prepareNum(opt.tels);
        // if(destination == null)
        //     return;
        // var smsCount = this.countSMS(opt.megs);
        var requestUrlStr = this.bashUrl+'destination='+encodeURIComponent(opt.tel)+'&source='+encodeURIComponent(opt.name)+'&message='+encodeURIComponent(opt.megs);
        console.log(requestUrlStr);
        if(processReport)
            return this.sendMessage(requestUrlStr, this.processResponds);

        return this.sendMessage(requestUrlStr, opt.cb);
    },

    processResponds: function (res) {
        var respondsArr = res.split(',');
        if(respondsArr.length == 1){

        }
    }
};

/*
{mess: "This is a text message"}
 */
