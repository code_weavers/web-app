/**
 * Created by Eric.Badger on 3/16/2017.
 */
module.exports = function (res, mgs) {
    res.status('500');
    var _msg = (mgs) ? mgs : 'System error contact the developers';
    res.json({msg: _msg});
};
