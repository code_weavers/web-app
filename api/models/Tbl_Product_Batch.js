module.exports = {
    attributes: {
        tbl_product_item: {model: "tbl_product_item"},
        tbl_batch: {model: "tbl_batch"},
        tbl_user: {model: "tbl_user"},
        tbl_product: {model: "tbl_product"}
    }
};

