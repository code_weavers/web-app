module.exports = {
    attributes: {
        name: {type: 'string'},
        description: {type: 'string'},
        tbl_product_item: {
            collection: 'tbl_product_item',
            via: 'tbl_product'
        },
        tbl_product_batch: {
            collection: 'tbl_product_batch',
            via: 'tbl_product'
        }
    }
};
