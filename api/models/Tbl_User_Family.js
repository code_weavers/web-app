module.exports = {
    attributes: {
        fa_name: {type: 'string'},
        fa_occupation: {type: 'string'},
        fa_nationality: {type: 'string'},
        mo_name: {type: 'string'},
        mo_occupation: {type: 'string'},
        mo_nationality: {type: 'string'},
        marital_status: {type: 'string'},
        num_children: {type: 'integer'},
        next_of_kin: {type: 'string'},
        nok_contact: {type: 'string'},
        tbl_user: {model: "tbl_user"}
    }
};
