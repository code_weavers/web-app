module.exports = {
    attributes: {
        role: {type: 'string'},
        weight: {type: 'integer'},
        tbl_login: {
            collection: 'tbl_login',
            via: 'tbl_role'
        }
    }
};
