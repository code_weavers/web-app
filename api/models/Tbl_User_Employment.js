module.exports = {
    attributes: {
        from: {type: 'date'},
        to: {type: 'date'},
        name: {type: 'string'},
        position: {type: 'string'},
        duties: {type: 'string'},
        reason_for_leaving: {type: 'string'},
        referee_name: {type: 'string'},
        referee_contact: {type: 'string'},
        tbl_user: {model: "tbl_user"}
    }
};
