module.exports = {
    attributes: {
        username: {type: 'string'},
        tbl_password: {model: "tbl_password"},
        tbl_role: {model: "tbl_role"},
        tbl_user: {model: 'tbl_user'},

        tbl_distributor_sales: {
            collection: 'tbl_distributor_sales',
            via: 'tbl_login'
        },
        tbl_distributor_agent: {
            collection: 'tbl_distributor_sales',
            via: 'tbl_login'
        },
        tbl_agent_sales: {
            collection: 'tbl_distributor_sales',
            via: 'tbl_login'
        }
    }
};
