module.exports = {
    attributes: {
        address: {type: 'string'},
        direction: {type: 'string'},
        email: {type: 'string'},
        tbl_user: {model: "tbl_user"}
    }
};
