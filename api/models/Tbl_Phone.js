module.exports = {
    attributes: {
        number: {type: 'string'},
        verified: {type: 'boolean'},
        code: {type: 'string'},
        expire: {type: 'datetime'},
        rank: {type: 'integer'},
        tbl_user: {model: "tbl_user"}
    }
};
