module.exports = {
    attributes: {
        type_of_edu: {type: 'string'},
        school: {type: 'string'},
        trainer: {type: 'string'},
        trainer_contact: {type: 'string'},
        from: {type: 'date'},
        to: {type: 'date'},
        tbl_user: {model: "tbl_user"}
    }
};
