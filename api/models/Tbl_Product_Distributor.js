module.exports = {
    attributes: {
        quantity: {type: 'integer'},
        left: {type: 'string'},
        confirm: {type: 'boolean'},
        tbl_user: {model: "tbl_user"},
        distributor: {model: 'tbl_user'},
        tbl_product_item: {model: "tbl_product_item"},

        tbl_product_agent: {
            collection: 'tbl_product_agent',
            via: 'tbl_product_distributor'
        },
        tbl_product_sales_person: {
            collection: 'tbl_product_sales_person',
            via: 'tbl_product_distributor'
        }
    }
};
