module.exports = {
    attributes: {
        serial: {type: 'string'},
        used: {type: 'boolean'}
    }
};