module.exports = {
    attributes: {
        image: {type: 'string'},
        name: {type: 'string'},
        tbl_product: {type: 'string'},
        price: {type: 'string'},
        telephone: {type: 'string'},
        payment_type: {type: 'string'},
        payment_ID: {type: 'string'},
        amount_paid: {type: 'string'},
        staff_ID: {type: 'string'},
        tbl_user: {model: 'tbl_user'},
        tbl_customer: {
            collection: 'tbl_customer',
            via: 'tbl_direct_cash'
        }
    }
};
