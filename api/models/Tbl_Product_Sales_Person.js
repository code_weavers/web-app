module.exports = {
    attributes: {
        quantity: {type: 'integer'},
        left: {type: 'string'},
        confirm: {type: 'boolean'},
        tbl_user: {model: "tbl_user"},
        sales_person: {model: 'tbl_user'},
        // tbl_product_item: {model: "tbl_product_item"}
        tbl_product_distributor: {model: "tbl_product_distributor"}
    }
};
