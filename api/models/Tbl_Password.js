var bcrypt = require('bcryptjs');
module.exports = {
    attributes: {
        pass: {type: 'string'},
        reset: {type: 'boolean'},
        tbl_login: {
            collection: 'tbl_login',
            via: 'tbl_password'
        }
    },

    // Lifecycle Callbacks
    beforeCreate: function (values, cb) {
        // Encrypt password
        bcrypt.hash(values.pass
            , 10, function (err, hash) {
                if (err) return cb(err);
                values.pass = hash;
                //calling cb() with an argument returns an error. Useful for canceling the entire operation if some criteria fails.
                cb();
            });
    },

    beforeUpdate: function (values, cb) {
        // Encrypt password
        bcrypt.hash(values.pass
            , 10, function (err, hash) {
                if (err) return cb(err);
                values.pass = hash;
                //calling cb() with an argument returns an error. Useful for canceling the entire operation if some criteria fails.
                cb();
            });
    }
};
