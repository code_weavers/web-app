module.exports = {
    attributes: {
        f_name: {type: 'string'},
        l_name: {type: 'string'},
        m_name: {type: 'string'},
        pix: {type: 'string'},
        dob: {type: 'date'},
        hometown: {type: 'string'},
        nationality: {type: 'string'},
        hobbies: {type: 'string'},
        profession: {type: 'string'},
        info: {type: 'string'},
        tbl_login: {
            collection: 'tbl_login',
            via: 'tbl_user'
        },
        tbl_phone: {
            collection: 'tbl_phone',
            via: 'tbl_user'
        },
        tbl_product_distributor: {
            collection: 'tbl_product_distributor',
            via: 'tbl_user'
        },
        tbl_product_distributor: {
            collection: 'tbl_product_distributor',
            via: 'distributor'
        },
        tbl_product_agent: {
            collection: 'tbl_product_agent',
            via: 'tbl_user'
        },
        tbl_product_agent: {
            collection: 'tbl_product_agent',
            via: 'agent'
        },
        tbl_product_sales_person: {
            collection: 'tbl_product_sales_person',
            via: 'tbl_user'
        },
        tbl_product_sales_person: {
            collection: 'tbl_product_sales_person',
            via: 'sales_person'
        },
        tbl_product_batch: {
            collection: 'tbl_product_batch',
            via: 'tbl_user'
        },

        tbl_agent_sales: {
            collection: 'tbl_agent_sales',
            via: 'agent'
        },
        tbl_agent_sales: {
            collection: 'tbl_agent_sales',
            via: 'sales_person'
        },

        tbl_distributor_sales: {
            collection: 'tbl_distributor_sales',
            via: 'distributor'
        },
        tbl_distributor_sales: {
            collection: 'tbl_distributor_sales',
            via: 'sales_person'
        },

        tbl_distributor_agent: {
            collection: 'tbl_distributor_agent',
            via: 'distributor'
        },
        tbl_distributor_agent: {
            collection: 'tbl_distributor_agent',
            via: 'agent'
        },
        tbl_user_employment: {
            collection: 'tbl_user_employment',
            via: 'tbl_user'
        },
        tbl_user_contact: {
            collection: 'tbl_user_contact',
            via: 'tbl_user'
        },
        tbl_user_education: {
            collection: 'tbl_user_education',
            via: 'tbl_user'
        },
        tbl_user_family: {
            collection: 'tbl_user_family',
            via: 'tbl_user'
        },

        tbl_product_item: {
            collection: 'tbl_product_item',
            via: 'tbl_user'
        }
    }
};
