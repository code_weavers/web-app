module.exports = {
    attributes: {
        quantity: {type: 'integer'},
        details: {type: 'string'},
        left: {type: 'integer'},
        tbl_product: {model: "tbl_product"},
        tbl_user: {model:'tbl_user'},
        price: {type:'float'},

        tbl_product_batch: {
            collection: 'tbl_product_batch',
            via: 'tbl_product_item'
        },
        tbl_product_distributor: {
            collection: 'tbl_product_distributor',
            via: 'tbl_product_item'
        }
        // tbl_product_agent: {
        //     collection: 'tbl_product_agent',
        //     via: 'tbl_product_item'
        // },
        // tbl_product_sales_person: {
        //     collection: 'tbl_product_sales_person',
        //     via: 'tbl_product_item'
        // }
    }
};
