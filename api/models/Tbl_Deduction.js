module.exports = {
    attributes: {
        image: {type: 'string'},
        organisation: {type: 'string'},
        staff_ID: {type: 'string'},
        name: {type: 'string'},
        tbl_product: {type: 'string'},
        price: {type: 'string'},
        monthly_deduction: {type: 'string'},
        telephone: {type: 'string'},
        work_place: {type: 'string'},
        doc_image: {type: 'string'},
        tbl_user: {model: 'tbl_user'},
        serial_number: {type: 'string'},
        tbl_customer: {
            collection: 'tbl_customer',
            via: 'tbl_deduction'
        }
    }
};
