module.exports = {
    attributes: {
        batch_num: {type: 'string'},
        details: {type: 'string'},
        tbl_product_batch: {
            collection: 'tbl_product_batch',
            via: 'tbl_batch'
        }
    }
};
