/**
 * HomeController
 *
 * @description :: Server-side logic for managing Logins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var bcrypt = require('bcryptjs');

module.exports = {
    index: function (req, res) {
        res.view("loginpage")
    },

    login: function (req, res) {
        if (req.method != "POST")
            return res.redirect('/');
        //collect variables and validate them
        var username = req.body['username'];
        var req_password = req.body.pass;
        if (username == '' || req_password == '') {
            req.flash('error', 'Enter your username and Password');
            return res.redirect('/');
        }

        Tbl_Login.findOne({username: username}).populate("tbl_role").populate('tbl_password').populate('tbl_user', 'id', 'f_name').exec(function (er, userLoginDetails) {
            if (!userLoginDetails) {
                req.flash('error', 'Check your password and username then try again');
                return res.redirect('/');
            } else {
                Tbl_Phone.findOne({
                    tbl_user: userLoginDetails.tbl_user.id,
                    rank: 1
                }).exec(function (phone_error, phone_record) {
                    // console.log(phone_record);
                    if (er || phone_error) {
                        //TODO: proper handling of errors for production and change 500.ejs page to reflect the company
                        console.log(e);
                        return res.view('500');
                    } else {
                        //without phone number the person should talk to the admin
                        // if(phone_record){
                        //     req.flash('error', 'Talk to the admin to add your phone number and try again');
                        //     return res.redirect('/');
                        // }
                        //user exist
                        if (userLoginDetails) {
                            //layout login
                            //compare password
                            bcrypt.compare(req_password, userLoginDetails.tbl_password.pass, function (e, r) {
                                if (e) {
                                    //TODO: proper handling of errors for production and change 500.ejs page to reflect the company
                                    console.log(e);
                                    return res.view('500');
                                }
                                if (r) {
                                    //comparing: if successful log the person in
                                    //check if reset and them redirect to reset
                                    if (userLoginDetails.tbl_password.reset) {
                                        req.session.role = userLoginDetails['tbl_role'].weight;
                                        req.session.role_name = userLoginDetails['tbl_role'].role;
                                        req.session.user = userLoginDetails.tbl_user.id;
                                        req.session.pass = userLoginDetails.tbl_password.id;
                                        req.session.verified = phone_record.verified;
                                        req.session.userFirstName = userLoginDetails.tbl_user.f_name;
                                        return res.redirect("/login/reset");

                                    } else {
                                        //set users session
                                        req.session.role = userLoginDetails['tbl_role'].weight;
                                        req.session.role_name = userLoginDetails['tbl_role'].role;
                                        //every user should have this session
                                        req.session.auth = true;
                                        req.session.user = userLoginDetails.tbl_user.id;
                                        req.session.verified = phone_record.verified;
                                        req.session.user_first_name = userLoginDetails.tbl_user.f_name;

                                        return res.redirect("/dashboard")
                                    }
                                }

                                req.flash('error', 'Check your password and username then try again');
                                return res.redirect('/');
                            });
                            //user don't exit
                        } else {
                            req.flash('error', 'Check your password and username then try again');
                            return res.redirect('/');
                        }
                    }
                });
            }
        })
    },

    reset: function (req, res) {
        console.log(req.body);
        if (req.method == "GET") {
            return res.view('reset');
        }

        //collect the reset data
            var new_pass = req.body.new_pass;
        var confirm_new_pass = req.body.cnew_pass;

        if (!new_pass && !confirm_new_pass) {
            req.flash('error', 'Password fields must not be blank');
            return res.redirect('/login/reset');
        }

        if (confirm_new_pass !== new_pass) {
            req.flash('error', 'Password does not match');
            return res.redirect('/login/reset');
        }

        //verify that the person has logged in first
        if (!req.session.user && req.session.pass) {
            return res.redirect('/');
        }

        else {
            //reset the users password
            Tbl_Password.update({id: req.session.pass}, {pass: new_pass, reset: false}).exec(function (e, r) {
                if (e) {
                    console.log(e);
                    return res.view('500');
                } else {
                    delete req.session.pass;
                    req.session.auth = true;
                    return res.redirect("/dashboard");
                }
            })

        }

    },

    verify_number: function (req, res) {
        if (req.method == "GET") {
            return Tbl_Phone.findOne({tbl_user: req.session.user, rank: 1}).exec(function (e, r) {
                if (e) {
                    console.log(e);
                    return res.json({result: false, error: e});
                } else {
                    req.session.phone_id = r.id;
                    return res.view('verify_num', r);
                }
            });

        } else {
            var code = req.body.code;
            var id = req.session.phone_id;

            Tbl_Phone.findOne({tbl_user: req.session.user, id: id}).exec(function (e, r) {
                if (e) {
                    console.log(e);
                    return res.json({result: false, error: e});
                } else if (r) {
                    if ((r.code != code)) {
                        return res.json({result: true, error: "mach", mess: "Code don't much"});
                    } else if (new Date() > new Date(r.expire)) {
                        return res.json({result: true, error: "expire", mess: "Code has expire, try again"});
                    } else {
                        Tbl_Phone.update({id: id}, {code: '', expire: '', verified: true}).exec(function (e, r) {
                            if (e) {
                                console.log(e);
                                return res.json({result: false, error: e});
                            } else {
                                req.session.verified = true;
                                return res.redirect("/dashboard");
                            }
                        })
                    }
                } else {
                    return res.json({result: true, error: "found", mess: 'Phone number not found'});
                }
            })
        }
    },

    send_verification_code: function (req, res) {
        var code = SecurityCode.getCode(6);
        var phone_id = req.session.phone_id;

        Tbl_Phone.update({id: phone_id}, {code: code, expire: new Date(Date.now() + 3600000)}).exec(function (e, r) {
            if (e) {
                console.log(e);
                return res.json({result: false, error: e});
            } else {
                console.log(r);
                SMSservice.send({
                    name: 'Herbaltech',
                    megs: 'Herbaltech Phone number verification code: ' + code,
                    tel: r[0].number,
                    // tel: '+233204050873',
                    cb: function (e) {
                        // console.log(e)
                    }
                }, false);
                return res.view('verify_form')
            }
        })
    },


    verify_code: function (req, res) {
        var code = req.body.verify_code;
        var phone_id = req.session.phone_id;
        var userid = req.session.user;

        if (!code) {
            req.flash('error', 'No code supplied !');
            return res.view('verify_form');
        }


        else {
            Tbl_Phone.update({id: phone_id, code: code}, {verified: 1}).exec(function (er, response) {
                if (er) {
                    console.log(er);
                    return res.send('500');
                } else if (response) {
                    req.session.verified = true;
                    return res.redirect("/dashboard");
                }
                else {
                    req.flash('error', 'Invalid code or expired');
                    return res.view('verify_form');
                }

            })
        }
    },


    logout: function (req, res) {
        if (req.session.auth)
            req.session.destroy();
        return res.redirect('/');
    },

    change_pass: function (req, res) {
        console.log(req.body, req.params.id);
        //find the id of the users password from the login table
        Tbl_Login.findOne({tbl_user: req.params.id}).exec(function (e, r) {
            if (e) {
                console.log(e);
                SendErrRes(res);
            } else if(r){
                Tbl_Password.update({id: r.tbl_password}, {pass: req.body.new_pass, reset: true}).exec(function (ee, rr) {
                    if (ee) {
                        console.log(ee);
                        SendErrRes(res);
                    } else {
                        return res.ok();
                    }
                })
            }
        })
    }
};


