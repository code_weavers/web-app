/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

// function buildWhile(req) {
//     return {tbl_role: {'>': (req.session.role == 1)? 4: (req.session.role == 2)? 3: (req.session.role == 3)? 2: (req.session.role == 4)? 0: 4}}
// }

var async = require('async');
var path = require('path');
module.exports = {
    index: function (req, res) {
        /**
         * accessible to only agents and above
         */
        // Tbl_Login.find({
        //     tbl_role: {
        //         '>': (req.session.role == 1) ? 4 :
        //             (req.session.role == 2) ? 3 :
        //                 (req.session.role == 3) ? 2 :
        //                     (req.session.role == 4) ? 0 : 4
        //     }, sort: 'tbl_role'
        // }).populate("tbl_user").populate("tbl_role").exec(function (e, r) {
        //     if (e) {
        //         console.log(e);
        //         res.status('500');
        //         res.json({msg: 'System error contact the developers'});
        //     } else {
        //         return res.json(r);
        //     }
        //
        // })

        //super admin
        if (req.session.role == 4) {
            Tbl_Login.find({sort: 'tbl_role'}).populate("tbl_user").populate("tbl_role").exec(function (e, r) {
                if (e) {
                    console.log(e);
                    res.status('500');
                    res.json({msg: 'System error contact the developers'});
                } else {
                    return res.json(r);
                }
            });
        }
        //route for admin
        else if (req.session.role == 3) {
            Tbl_Login.find({
                tbl_role: {'>': 2},
                sort: 'tbl_role'
            }).populate("tbl_user").populate("tbl_role").exec(function (e, r) {
                if (e) {
                    console.log(e);
                    res.status('500');
                    res.json({msg: 'System error contact the developers'});
                } else {
                    return res.json(r);
                }
            });
        }
        //route for distributor
        else if (req.session.role == 2) {
            Tbl_Distributor_Agent.find({
                distributor: req.session.user
            }).populate("agent").populate('tbl_login').exec(function (e, r) {
                if (e) {
                    console.log(e);
                    res.status('500');
                    res.json({msg: 'System error contact the developers'});
                } else
                    Tbl_Distributor_Sales.find({
                        distributor: req.session.user
                    }).populate("sales_person").populate('tbl_login').exec(
                        function (ee, rr) {
                            if (ee) {
                                console.log(ee);
                                res.status('500');
                                res.json({msg: 'System error contact the developers'});
                            } else {
                                return res.json({agent: r, sales: rr});
                            }
                        });
            })
        }
        //route agent
        else if (req.session.role == 1) {
            Tbl_Agent_Sales.find({
                agent: req.session.user
            }).populate("sales_person").populate('tbl_login').exec(function (e, r) {
                if (e) {
                    console.log(e);
                    res.status('500');
                    res.json({msg: 'System error contact the developers'});
                } else
                    // console.log(r)
                return res.json(r);
            })
        } else
            return res.ok()
    },

    create: function (req, res) {
        // console.log(res.body, 'body');
        var personal_data = req.body.personal_data;
        var contact = req.body.contact;
        var edu = req.body.edu;
        var emply = req.body.emply;
        var family = req.body.family;
        var login = req.body.login;
        // console.log(personal_data)
        // TODO: verfiy that the user has the permission to create that user
        create_user(personal_data, res, function (r) {
            var id = r.id;
            user_contact(contact, id, res, function (r) {
                // console.log(id);
                user_edu(edu, id, res, function (r) {
                    user_emply(emply, id, res, function (r) {
                        user_family(family, id, res, function (r) {
                            register(login, id, res, function (l) {

                                //login is a distributor
                                if (req.session.role == 2) {
                                    //user created is an agent
                                    if (login.role == 4) {
                                        Tbl_Distributor_Agent.create({
                                            distributor: req.session.user,
                                            agent: id,
                                            tbl_login: l.id
                                        }).exec(function (ee, rr) {
                                            if (ee) {
                                                console.log(ee);
                                                res.status('500');
                                                res.json({msg: 'System error contact the developers'});
                                            } else {
                                                return res.ok();
                                            }
                                        })
                                    }
                                    //user created is a distributor
                                    if (login.role == 5) {
                                        // console.log('i am here')
                                        Tbl_Distributor_Sales.create({
                                            distributor: req.session.user,
                                            sales_person: id,
                                            tbl_login: l.id
                                        }).exec(function (ee, rr) {
                                            if (ee) {
                                                console.log(ee);
                                                res.status('500');
                                                res.json({msg: 'System error contact the developers'});
                                            } else {
                                                // console.log("sales person", rr)
                                                return res.ok();
                                            }
                                        })
                                    }
                                }
                                else if (req.session.role == 1) {
                                    //user created is a distributor
                                    if (login.role == 5) {
                                        Tbl_Agent_Sales.create({
                                            agent: req.session.user,
                                            sales_person: id,
                                            tbl_login: l.id
                                        }).exec(function (ee, rr) {
                                            if (ee) {
                                                console.log(ee);
                                                res.status('500');
                                                res.json({msg: 'System error contact the developers'});
                                            } else {
                                                // console.log(rr)
                                                return res.ok();
                                            }
                                        })
                                    }
                                }else
                                    res.ok();
                            })
                        })
                    })
                })
            })
        })
    },


    check_username: function (req, res) {
        var username = req.params.id;
        Tbl_Login.find({username: username}).exec(function (e, r) {
            if (r.length < 1)
                return res.json({result: false});
            else
                return res.json({result: true});
        })
    }
    ,

    get_login_user: function (req, res) {
        return res.json({
            user: req.session.user_first_name,
            role: req.session.role_name,
            role_weight: req.session.role
        });
    }
    ,

    get_user: function (req, res) {
        var user_id = req.params.id;
        async.parallel({
            login: function (cb) {
                Tbl_Login.findOne({tbl_user: user_id}).populate('tbl_role').exec(function (e, r) {
                    if (e)
                        cb(e);
                    else
                        cb(null, r);
                })
            },
            user: function (cb) {
                Tbl_User.findOne({id: user_id}).exec(function (e, r) {

                    if (e)
                        cb(e, null);
                    else
                        cb(null, r);
                })
            },
            contact: function (cb) {
                Tbl_User_Contact.findOne({tbl_user: user_id}).exec(function (e, r) {

                    if (e)
                        cb(e, null);
                    else
                        cb(null, r);
                })
            },
            education: function (cb) {
                Tbl_User_Education.findOne({tbl_user: user_id}).exec(function (e, r) {

                    if (e)
                        cb(e, null);
                    else
                        cb(null, r);
                })
            },
            emply: function (cb) {
                Tbl_User_Employment.findOne({tbl_user: user_id}).exec(function (e, r) {

                    if (e)
                        cb(e, null);
                    else
                        cb(null, r);
                })
            },
            family: function (cb) {
                Tbl_User_Family.findOne({tbl_user: user_id}).exec(function (e, r) {

                    if (e)
                        cb(e, null);
                    else
                        cb(null, r);
                })
            },
            // product: function (cb) {
            //     Tbl_Distribution
            // }
        }, function (ee, rr) {
            if (ee) {
                console.log(ee);
                SendErrRes(res);
            } else {
                return res.jsonx(rr)
            }
        });
    },

    update_user: function (req, res) {
        // console.log(req.body);
        var user_id = req.params.id;
        var func = [];
        if ("user" in req.body) {
            func.push(function (cb) {
                Tbl_User.findOne({id: user_id}).exec(function (ee, rr) {
                    if (ee)
                        cb(ee, null);
                    else if (rr) {
                        //it exist update
                        rr.f_name = req.body.user.f_name;
                        rr.l_name = req.body.user.l_name;
                        rr.m_name = req.body.user.m_name;
                        rr.pix = req.body.user.pix;
                        rr.dob = req.body.user.dob;
                        rr.hometown = req.body.user.hometown;
                        rr.nationality = req.body.user.nationality;
                        rr.hobbies = req.body.user.hobbies;
                        rr.profession = req.body.user.profession;
                        rr.info = req.body.user.info;
                        rr.save(function (e) {
                            cb(e)
                        })
                    } else {
                        Tbl_User.create({
                            f_name: req.body.user.f_name,
                            l_name: req.body.user.l_name,
                            m_name: req.body.user.m_name,
                            pix: req.body.user.pix,
                            dob: req.body.user.dob,
                            hometown: req.body.user.hometown,
                            nationality: req.body.user.nationality,
                            hobbies: req.body.user.hobbies,
                            profession: req.body.user.profession,
                            info: req.body.user.info,
                            tbl_user: user_id
                        }).exec(function (e, r) {
                            if (e)
                                cb(e, null);
                            else
                                cb(null, r);
                        })
                    }
                })
            })
        }
        if ("contact" in req.body) {
            func.push(function (cb) {
                Tbl_User_Contact.findOne({tbl_user: user_id}).exec(function (e, r) {
                    if (e)
                        cb(e, null);
                    else if (r) {
                        r.address = req.body.contact.address;
                        r.direction = req.body.contact.direction;
                        r.email = req.body.contact.email;
                        r.save(function (ee) {
                            cb(ee);
                        })
                    } else {
                        Tbl_User_Contact.create({
                            address: req.body.contact.address,
                            direction: req.body.contact.direction,
                            email: req.body.contact.email,
                            tbl_user: user_id
                        }).exec(function (e, r) {
                            if (e)
                                cb(e, null);
                            else
                                cb(null, r);
                        })
                    }
                })

            })
        }
        if ("education" in req.body) {
            func.push(function (cb) {
                Tbl_User_Education.findOne({tbl_user: user_id}).exec(function (e, r) {
                    if (e)
                        cb(e, null);
                    else if (r) {
                        r.type_of_edu = req.body.education.type_of_edu;
                        r.school = req.body.education.school;
                        r.trainer = req.body.education.trainer;
                        r.trainer_contact = req.body.education.trainer_contact;
                        r.save(function (ee) {
                            cb(ee);
                        })
                    } else {
                        Tbl_User_Education.create({
                            type_of_edu: req.body.education.type_of_edu,
                            school: req.body.education.school,
                            trainer: req.body.education.trainer,
                            trainer_contact: req.body.education.trainer_contact,
                            tbl_user: user_id
                        }).exec(function (e, r) {
                            if (e)
                                cb(e, null);
                            else
                                cb(null, r);
                        })
                    }
                })

            })
        }
        if ("emply" in req.body) {
            func.push(function (cb) {
                Tbl_User_Employment.findOne({tbl_user: user_id}).exec(function (e, r) {
                    if (e)
                        cb(e, null);
                    else if (r) {
                        r.from = req.body.emply.from;
                        r.to = req.body.emply.to;
                        r.name = req.body.emply.name;
                        r.position = req.body.emply.position;
                        r.duties = req.body.emply.duties;
                        r.reason_for_leaving = req.body.emply.reason_for_leaving;
                        r.referee = req.body.emply.referee;
                        r.save(function (ee) {
                            cb(ee)
                        })
                    } else {
                        Tbl_User_Employment.create({
                            from: req.body.emply.from,
                            to: req.body.emply.to,
                            name: req.body.emply.name,
                            position: req.body.emply.position,
                            duties: req.body.emply.duties,
                            reason_for_leaving: req.body.emply.reason_for_leaving,
                            referee: req.body.emply.referee,
                            tbl_user: user_id
                        }).exec(function (e, r) {
                            if (e)
                                cb(e, null);
                            else
                                cb(null, r);
                        })
                    }
                })
            })
        }
        if ("family" in req.body) {
            func.push(function (cb) {
                Tbl_User_Family.findOne({tbl_user: user_id}).exec(function (e, r) {
                    if (e)
                        cb(e, null);
                    else if (r) {
                        r.fa_name = req.body.family.fa_name;
                        r.fa_occupation = req.body.family.fa_occupation;
                        r.fa_nationality = req.body.family.fa_nationality;
                        r.mo_name = req.body.family.mo_name;
                        r.mo_occupation = req.body.family.mo_occupation;
                        r.mo_nationality = req.body.family.mo_nationality;
                        r.marital_status = req.body.family.marital_status;
                        r.num_children = req.body.family.num_children;
                        r.next_of_kin = req.body.family.next_of_kin;
                        r.nok_contact = req.body.family.ok_contact;
                        r.save(function (ee) {
                            cb(ee);
                        })
                    } else {
                        Tbl_User_Family.create({
                            fa_name: req.body.family.fa_name,
                            fa_occupation: req.body.family.fa_occupation,
                            fa_nationality: req.body.family.fa_nationality,
                            mo_name: req.body.family.mo_name,
                            mo_occupation: req.body.family.mo_occupation,
                            mo_nationality: req.body.family.mo_nationality,
                            marital_status: req.body.family.marital_status,
                            num_children: req.body.family.num_children,
                            next_of_kin: req.body.family.next_of_kin,
                            nok_contact: req.body.family.ok_contact
                        }).exec(function (e, r) {
                            if (e)
                                cb(e, null);
                            else
                                cb(null, r);
                        })
                    }
                })

            })
        }
        //run the func in parallel
        async.parallel(
            func , function (err, r) {
                if (err) {
                    console.log(err);
                    SendErrRes(res);
                } else {
                    return res.jsonx(r)
                }
            }
        )
    },

    upload: function (req, res) {
        var user_id = req.params.id;
        req.file('pix').upload({dirname: '../../assets/upload'}, function (err, uploaded_file) {
            if (err)
                console.log(err);
            else {
                var file_name = path.basename(uploaded_file[0].fd);
                Tbl_User.update({id: user_id},{pix: file_name}).exec(function (e, r) {
                    if (e) {
                        console.log(e);
                        return SendErrRes(res);
                    } else {
                        return res.ok();
                    }
                })
            }
        })
    }

};


function create_user(data, res, cb) {
    /**
     * @description :: first route in creating a new user. Responsible for populating the
     *                  user table. receives json only
     */

        //collect the variables for post and validate them
    var f_name = data.f_name;
    var l_name = data.l_name;
    var m_name = data.m_name;
    var pix = data.pix;
    var dob = data.dob;
    var hometown = data.hometown;
    var nationality = data.nationality;
    var hobbies = data.hobbies;
    var profession = data.profession;
    var info = data.info;

    //check required variables
    if (!f_name || !l_name || !dob || !hometown || !nationality || !hobbies || !profession) {
        return SendErrRes(res, 'All fields are required');
    }

    //create the use and return its id
    Tbl_User.create({
        f_name: f_name,
        l_name: l_name,
        m_name: m_name,
        pix: pix,
        dob: dob,
        hometown: hometown,
        nationality: nationality,
        hobbies: hobbies,
        profession: profession,
        info: info
    }).exec(function (e, r) {
        if (e) {
            console.log(e);
            return SendErrRes(res);
        } else {
            cb(r);
        }
    })
}

function user_contact(data, _user_id, res, cb) {
    var user_id = _user_id;
    var address = data.address;
    var direction = data.direction;
    var email = data.email;
    var number = data.phone;

    Tbl_User_Contact.create({
        address: address,
        direction: direction,
        email: email,
        tbl_user: user_id
    }).exec(function (e, r) {
        if (e) {
            return SendErrRes(res);
        } else {
            Tbl_Phone.create({tbl_user: user_id, number: number, verified: false, rank: 1}).exec(function (e, r) {
                if (e) {
                    return SendErrRes(res);
                } else {
                    cb(r);
                }
            })
        }
    })
};

function user_edu(data, user_id, res, cb) {
    var type_of_edu = data.type_of_edu;
    var school = data.school;
    var trainer = data.trainer;
    var trainer_contact = data.trainer_contact;

    Tbl_User_Education.create({
        tbl_user: user_id,
        type_of_edu: type_of_edu,
        school: school,
        trainer: trainer,
        trainer_contact: trainer_contact
    }).exec(function (e, r) {
        if (e) {
            SendErrRes(res);
        } else {
            cb(r);
        }
    })
};

function user_emply(data, user_id, res, cb) {
    var from = data.from;
    var to = data.to;
    var name = data.name;
    var position = data.position;
    var duties = data.duties;
    var reason_for_leaving = data.reason_for_leaving;
    var referee = data.referee;

    Tbl_User_Employment.create({
        from: from,
        to: to,
        name: name,
        position: position,
        duties: duties,
        reason_for_leaving: reason_for_leaving,
        referee: referee,
        tbl_user: user_id
    }).exec(function (e, r) {
        if (e) {
            console.log(e);
            SendErrRes(res);
        } else {
            cb(r);
        }
    })

};

function user_family(data, user_id, res, cb) {
    var fa_name = data.fa_name;
    var fa_occupation = data.fa_occupation;
    var fa_nationality = data.fa_nationality;
    var mo_name = data.mo_name;
    var mo_occupation = data.mo_occupation;
    var mo_nationality = data.mo_nationality;
    var marital_status = data.marital_status;
    var num_children = data.num_children;
    var next_of_kin = data.next_of_kin;
    var nok_contact = data.nok_contact;

    Tbl_User_Family.create({
        fa_name: fa_name,
        fa_occupation: fa_occupation,
        fa_nationality: fa_nationality,
        mo_name: mo_name,
        mo_occupation: mo_occupation,
        mo_nationality: mo_nationality,
        marital_status: marital_status,
        num_children: num_children,
        next_of_kin: next_of_kin,
        nok_contact: nok_contact,
        tbl_user: user_id
    }).exec(function (e, r) {
        if (e) {
            console.log(e);
            SendErrRes(res)
        } else {
            cb();
        }
    })
}


function register(data, _user_id, res, cb) {
    var password = data.password;
    var username = data.username;
    var role = data.role;
    var user_id = _user_id;

    Tbl_Password.create({pass: password, reset: true}).exec(function (e, r) {
        if (e) {
            console.log(e);
            SendErrRes(res);
        } else {
            Tbl_Login.create({
                username: username,
                tbl_role: role,
                tbl_user: user_id,
                tbl_password: r.id
            }).exec(function (e, r) {
                if (e) {
                    console.log(e);
                    SendErrRes(res);
                } else {
                    cb(r)
                }
            })
        }
    })
}

function mergeJson(o, ob) {
    for (var z in ob) {
        o[z] = ob[z];
    }
    return o;
}

function hasKey(obj, key) {
    return key in obj;
}
