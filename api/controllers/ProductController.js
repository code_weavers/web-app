/**
 * ProductController
 *
 * @description :: Server-side logic for managing products
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var async = require('async');
module.exports = {
    // route for /product/
    index: function (req, res) {
        if (req.session.role > 2) {
            Tbl_Product_Batch.find().populate("tbl_product_item").populate("tbl_batch").populate("tbl_user").populate("tbl_product").exec(function (e, r) {
                if (e) {
                    return SendErrRes(res);
                } else
                    return res.json(r);
            });
        } else if (req.session.role == 2) {
            //distributor
            Tbl_Product_Distributor.find({distributor: req.session.user}).populate("tbl_product_item").populate("tbl_user").exec(function (e, r) {
                if (e) {
                    return SendErrRes(res);
                } else {
                    async.map(r, function (val, cb) {
                        Tbl_Product.findOne({id: val.tbl_product_item.tbl_product}).exec(function (err, relt) {
                            if (err) {
                                console.log(err);
                                cb(err);
                            } else {
                                val.product = relt;
                                cb(null, val)
                            }
                        })
                    }, function (e, r) {
                        if (e) {
                            console.log(e);
                            return SendErrRes(res);
                        } else {
                            return res.json(r);
                        }
                    })
                }
            });
        } else if (req.session.role == 1) {
            //agent
            Tbl_Product_Agent.find({agent: req.session.user}).populate("tbl_product_distributor").populate("tbl_user").exec(function (e, r) {
                if (e) {
                    return SendErrRes(res);
                } else {
                    async.map(r, function (val, cb) {
                        Tbl_Product_Item.findOne({id: val.tbl_product_distributor.tbl_product_item}).exec(function (err, relt) {
                            if (err) {
                                console.log(err);
                                cb(err);
                            } else {
                                Tbl_Product.findOne({id: relt.tbl_product}).exec(function (err, relt1) {
                                    if (err) {
                                        console.log(err);
                                        cb(err);
                                    } else {
                                        val.product = relt1;
                                        val.product_item = relt
                                        cb(null, val)
                                    }
                                })
                            }
                        })
                    }, function (e, r) {
                        if (e) {
                            console.log(e);
                            return SendErrRes(res);
                        } else {
                            return res.json(r);
                        }
                    })
                }
            });
        } else {
            //send error
            return SendErrRes(res);
        }
    },
    create: function (req, res) {
        var product_info = req.body;
        // TODO: verfiy that the user has the permission to create that user
        create_product(product_info, res, function (r) {
        })
    },

    product_name: function (req, res) {
        Tbl_Product.find().exec(function (e, r) {
            if (e) {
                return SendErrRes(res);
            } else
                return res.json(r);
        });
    },

    batch_number: function (req, res) {
        Tbl_Batch.find().exec(function (e, r) {
            if (e) {
                return SendErrRes(res);
            } else
                return res.json(r);
        });
    },

    distributor: function (req, res) {
        Tbl_Login.find({tbl_role: 3}).populate("tbl_user").populate("tbl_role").exec(function (e, r) {
            if (e) {
                return SendErrRes(res);
            } else {
                return res.json(r);
            }
        });
    },

    list_of_products: function (req, res) {
        Tbl_Product.find().exec(function (e, r) {
            if (e) {
                return SendErrRes(res);
            } else {
                return res.json(r);
            }
        })
    },

    create2: function (req, res) {
        var product_info = req.body;
        // TODO: verfiy that the user has the permission to create that user
        create_product_details(product_info, req, res, function (r) {
            Tbl_Product_Batch.create({
                tbl_product_item: r.id,
                tbl_batch: product_info.batch_id,
                tbl_user: req.session.user,
                tbl_product: product_info.name
            }).exec(function (e, r) {
                if (e) {
                    return SendErrRes(res);
                } else {
                    return res.ok();
                }
            });
        })
    },

    issue_product: function (req, res) {
        // TODO: verfiy that the user has the permission to create that user
        /**
         * @description :: first route in creating a new distributor. Responsible for populating the
         *                  product distributor table. receives json only
         */
            //collect the variables for post and validate them;
            // console.log(req.body)
        var quantity = req.body.quantity;
        var distributor = req.body.distributor;
        var tbl_product_item = req.body.product_id;
        //check required variables
        if (!quantity || !distributor) {
            return SendErrRes(res, 'All fields are required');
        }

        //create the product and return its id
        Tbl_Product_Distributor.create({
            distributor: distributor,
            quantity: quantity,
            tbl_user: req.session.user,
            tbl_product_item: tbl_product_item,
            left: quantity,
            confirm: false
        }).exec(function (e, r) {
            if (e) {
                return SendErrRes(res);
            } else {
                // console.log(distribute);
                if (e) {
                    return SendErrRes(res);
                } else {
                    Tbl_Product_Item.findOne({id: tbl_product_item}).exec(function (ee, rr) {
                        // console.log(r);
                        rr.left = rr.left - quantity;
                        rr.save(function (ee) {
                            if (ee) {
                                return SendErrRes(res);
                            } else {
                                return res.ok();
                            }
                        });

                    })
                }
            }
        });
    },

    create_batch: function (req, res) {
        /**
         * @description :: first route in creating a new batch. Responsible for populating the
         *                  batch table. receives json only
         */

            //collect the variables for post and validate them
        var batch_num = req.body.batch_num;
        var details = req.body.details;
        //check required variables
        if (!batch_num || !details) {
            return SendErrRes(res, 'All fields are required');
        }

        //create the product and return its id
        Tbl_Batch.create({
            batch_num: batch_num,
            details: details
        }).exec(function (e, r) {
            if (e) {
                console.log(e);
                return SendErrRes(res);
            } else {
                return res.json(r)
            }
        })
    },

    //route for /product/details
    details: function (req, res) {
        if (req.session.role > 2) {
            Tbl_Product_Batch.findOne({tbl_product_item: req.params.id}).populate("tbl_product_item").populate("tbl_batch").populate("tbl_user").populate("tbl_product").exec(function (e, r) {
                // console.log(r);
                if (e) {
                    console.log(e);
                    return SendErrRes(res);
                } else
                    Tbl_Product_Distributor.find({tbl_product_item: req.params.id}).populate("distributor").exec(function (ee, rr) {
                        if (e) {
                            return SendErrRes(res);
                        } else {
                            if (rr)
                                r.distributors = rr;
                            else
                                r.distributors = [];
                            // console.log(r)
                            return res.json(r);
                        }
                    });
            });
        } else if (req.session.role == 2) {
            //distributor
            Tbl_Product_Distributor.findOne({id: req.params.id}).populate("tbl_product_item").populate("tbl_user").exec(function (e, r) {
                // console.log(r);
                if (e) {
                    console.log(e);
                    return SendErrRes(res);
                } else {
                    async.parallel({
                        agent: function (cb) {
                            Tbl_Product_Agent.find({tbl_product_distributor: req.params.id}).populate("agent").exec(function (ee, rr) {
                                if (e) {
                                    return SendErrRes(res);
                                } else {
                                    cb(null, rr);
                                }
                            })
                        },
                        sales_person: function (cb) {
                            Tbl_Product_Sales_Person.find({tbl_product_distributor: req.params.id}).populate("sales_person").exec(function (ee, rr) {
                                if (e) {
                                    return SendErrRes(res);
                                } else {
                                    cb(null, rr);
                                }
                            })
                        },
                        product: function (cb) {
                            Tbl_Product.findOne({id: r.tbl_product_item.tbl_product}).exec(function (err, rr) {
                                if (e) {
                                    return SendErrRes(res);
                                } else {
                                    // console.log(rr)
                                    cb(null, rr);
                                }
                            })
                        }
                    }, function (e, relt) {
                        if (e) {
                            console.log(e);
                            return SendErrRes(res);
                        } else {
                            relt.dis_product = r;
                            return res.json(relt);
                        }
                    })
                }
            });

        } else if (req.session.role == 1) {
            //agent
            Tbl_Product_Agent.findOne({id: req.params.id}).populate("tbl_product_distributor").populate("tbl_user").exec(function (e, r) {
                // console.log(r);
                if (e) {
                    console.log(e);
                    return SendErrRes(res);
                } else {
                    async.parallel({
                        sales_person: function (cb) {
                            Tbl_Product_Sales_Person.find({tbl_product_distributor: r.tbl_product_distributor}).populate("sales_person").exec(function (ee, rr) {
                                if (e) {
                                    return SendErrRes(res);
                                } else {
                                    cb(null, rr);
                                }
                            })
                        },
                        product_item: function (cb) {
                            Tbl_Product_Item.findOne({id: r.tbl_product_distributor.tbl_product_item}).exec(function (err, relt1) {
                                if (err) {
                                    console.log(err);
                                    cb(err);
                                } else {
                                    Tbl_Product.findOne({id: relt1.tbl_product}).exec(function (err, relt2) {
                                        if (err) {
                                            console.log(err);
                                            cb(err);
                                        } else {
                                            relt1.product = relt2;
                                            cb(null, relt1)
                                        }
                                    })
                                }
                            })
                        }
                    }, function (e, relt) {
                        if (e) {
                            console.log(e);
                            return SendErrRes(res);
                        } else {
                            relt.dis_product = r;
                            return res.json(relt);
                        }
                    })
                }
            });
        } else {
            //send error
            return SendErrRes(res);
        }
    },

    get_users_product: function (req, res) {
        Tbl_Login.findOne({tbl_user: req.params.id}).exec(function (e, r) {
            // console.log(r)
            if (e) {
                console.log(e);
                return SendErrRes(res);
            } else {
                if (r.tbl_role == 3) {
                    //the user is a distributor
                    Tbl_Product_Distributor.find({distributor: req.params.id}).populate("tbl_user").populate('tbl_product_item').exec(function (ee, rr) {
                        if (e) {
                            console.log(e);
                            return SendErrRes(res);
                        } else {
                            // console.log(rr)
                            async.map(rr, function (val, cb) {
                                Tbl_Product.findOne({id: val.tbl_product_item.tbl_product}).exec(function (err, relt) {
                                    if (err) {
                                        console.log(err);
                                        cb(err);
                                    } else {
                                        val.product = relt;
                                        cb(null, val)
                                    }
                                })
                            }, function (e, r) {
                                if (e) {
                                    console.log(e);
                                    return SendErrRes(res);
                                } else {
                                    return res.json(r);
                                }
                            })
                        }
                    });
                } else if (r.tbl_role == 4) {
                    //user is an agent
                    Tbl_Product_Agent.find({agent: req.params.id}).populate("tbl_user").populate("agent").populate('tbl_product_distributor').exec(function (ee, rr) {
                        if (e) {
                            console.log(e);
                            return SendErrRes(res);
                        } else {
                            async.map(rr, function (val, cb) {
                                Tbl_Product_Item.findOne({id: val.tbl_product_distributor.tbl_product_item}).exec(function (err, relt1) {
                                    if (err) {
                                        console.log(e);
                                        return SendErrRes(res);
                                    } else {
                                        Tbl_Product.findOne({id: relt1.tbl_product}).exec(function (err, relt2) {
                                            if (err) {
                                                console.log(err);
                                                cb(err);
                                            } else {
                                                val.product = relt2;
                                                val.product_item = relt1;
                                                cb(null, val)
                                            }
                                        })
                                    }
                                })
                            }, function (e, r) {
                                if (e) {
                                    console.log(e);
                                    return SendErrRes(res);
                                } else {
                                    return res.json(r);
                                }
                            })
                        }
                    });
                } else if (r.tbl_role == 5) {
                    //user is a sales person
                    Tbl_Product_Sales_Person.find({sales_person: req.params.id}).populate("tbl_user").populate("sales_person").populate('tbl_product_distributor').exec(function (ee, rr) {
                        if (ee) {
                            console.log(ee);
                            return SendErrRes(res);
                        } else {
                            async.map(rr, function (val, cb) {
                                Tbl_Product_Item.findOne({id: val.tbl_product_distributor.tbl_product_item}).exec(function (err, relt1) {
                                    if (err) {
                                        console.log(e);
                                        return SendErrRes(res);
                                    } else {
                                        Tbl_Product.findOne({id: relt1.tbl_product}).exec(function (err, relt2) {
                                            if (err) {
                                                console.log(err);
                                                cb(err);
                                            } else {
                                                val.product = relt2;
                                                val.product_item = relt1;
                                                cb(null, val)
                                            }
                                        })
                                    }
                                })
                            }, function (e, r) {
                                if (e) {
                                    console.log(e);
                                    return SendErrRes(res);
                                } else {
                                    return res.json(r);
                                }
                            })

                        }
                    });
                } else {
                    //error user don't exist
                    console.log('user not found');
                    return SendErrRes(res);
                }
            }
        });
    },


    batch: function (req, res) {
        /**
         * this action is used to return the details of a batch
         * batch_id: is the id of the batch which has been clicked
         */

        var batch_id = req.params.id;
        Tbl_Product_Batch.find({tbl_batch: batch_id}).populate('tbl_product_item').populate('tbl_batch').populate('tbl_user').populate('tbl_product').exec(function (e, r) {
            if (e) {
                console.log(e);
                return SendErrRes(res);
            } else
                return res.json(r);
        })
    },

    confirm: function (req, res) {
        var id = req.params.id;
        if (req.session.role == 2) {
            Tbl_Product_Distributor.findOne({id: id}).exec(function (e, r) {
                if (e) {
                    console.log(e);
                    return SendErrRes(res);
                } else {
                    r.confirm = true;
                    r.save(function (e) {
                        if (e) {
                            console.log(e);
                            return SendErrRes(res);
                        } else
                            return res.json(r);
                    })
                }
            })
        } else if (req.session.role == 1) {
            Tbl_Product_Agent.findOne({id: id}).exec(function (e, r) {
                if (e) {
                    console.log(e);
                    return SendErrRes(res);
                } else {
                    r.confirm = true;
                    r.save(function (e) {
                        if (e) {
                            console.log(e);
                            return SendErrRes(res);
                        } else
                            return res.json(r);
                    })
                }
            })
        }
    },

    my_users: function (req, res) {
        //route for distributor
        if (req.session.role == 2) {
            Tbl_Distributor_Agent.find({
                distributor: req.session.user
            }).populate("agent").populate('tbl_login').exec(function (e, r) {
                if (e) {
                    console.log(e);
                    res.status('500');
                    res.json({msg: 'System error contact the developers'});
                } else
                    Tbl_Distributor_Sales.find({
                        distributor: req.session.user
                    }).populate("sales_person").populate('tbl_login').exec(
                        function (ee, rr) {
                            if (ee) {
                                console.log(ee);
                                res.status('500');
                                res.json({msg: 'System error contact the developers'});
                            } else {
                                return res.json({agent: r, sales: rr});
                            }
                        });
            })
        }
        //route agent
        else if (req.session.role == 1) {
            Tbl_Agent_Sales.find({
                agent: req.session.user
            }).populate("sales_person").populate('tbl_login').exec(function (e, r) {
                if (e) {
                    console.log(e);
                    res.status('500');
                    res.json({msg: 'System error contact the developers'});
                } else
                // console.log(r)
                    return res.json({sales: r});
            })
        } else
            return res.ok()
    },

    dis_issue: function (req, res) {
        var quantity = req.body.quantity;
        var user = req.body.user_id.split(',')[0];
        var role = req.body.user_id.split(',')[1];

        // console.log(req.body, quantity, user, role);
        if (req.session.role == 2) {
            if (role == 1) {
                Tbl_Product_Agent.create({
                    quantity: quantity,
                    available: quantity,
                    confirm: false,
                    tbl_user: req.session.user,
                    agent: user,
                    tbl_product_distributor: req.body.product
                }).exec(function (e, r) {
                    if (e) {
                        console.log(e);
                        return SendErrRes(res);
                    } else
                        Tbl_Product_Distributor.findOne({id: req.body.product}).exec(function (ee, rr) {
                            rr.left = rr.left - quantity;
                            rr.save(function (err) {
                                return res.json(r);
                            })
                        })
                });
            } else if (role == 2) {
                Tbl_Product_Sales_Person.create({
                    quantity: quantity,
                    left: quantity,
                    confirm: false,
                    tbl_user: req.session.user,
                    sales_person: user,
                    tbl_product_distributor: req.body.product
                }).exec(function (e, r) {
                    if (e) {
                        console.log(e);
                        return SendErrRes(res);
                    } else
                        Tbl_Product_Distributor.findOne({id: req.body.product}).exec(function (ee, rr) {
                            rr.left = rr.left - quantity;
                            rr.save(function (err) {
                                return res.json(r);
                            })
                        })
                })
            }
        } else if (req.session.role == 1) {
            Tbl_Product_Sales_Person.create({
                quantity: quantity,
                left: quantity,
                confirm: false,
                tbl_user: req.session.user,
                sales_person: user,
                tbl_product_distributor: req.body.product
            }).exec(function (e, r) {
                if (e) {
                    console.log(e);
                    return SendErrRes(res);
                } else
                    Tbl_Product_Agent.findOne({id: req.body.product}).exec(function (ee, rr) {
                        rr.left = rr.left - quantity;
                        rr.save(function (err) {
                            return res.json(r);
                        })
                    })
            })
        }
    },

    dis_product_distribution: function (req, res) {
        async.parallel({
            agent: function (cb) {
                Tbl_Product_Agent.find({tbl_product_distributor: req.params.id}).populate('agent').exec(function (e, r) {
                    if (e) {
                        console.log(e);
                        cb(e)
                    } else {
                        cb(null, r)
                    }
                })
            },
            sales_person: function (cb) {
                Tbl_Product_Sales_Person.find({tbl_product_distributor: req.params.id}).populate('sales_person').exec(function (e, r) {
                    if (e) {
                        console.log(e);
                        cb(e)
                    } else {
                        cb(null, r)
                    }
                })
            }
        }, function (e, r) {
            if (e) {
                console.log(e);
                return SendErrRes(res);
            } else {
                return res.json(r);
            }
        });
    }
};

function create_product(data, res, cb) {
    /**
     * @description :: first route in creating a new product. Responsible for populating the
     *                  product table. receives json only
     */

        //collect the variables for post and validate them;
    var name = data.name;
    var description = data.description;

    //check required variables
    if (!name || !description) {
        return SendErrRes(res, 'All fields are required');
    }

    //create the product and return its id
    Tbl_Product.create({
        name: name,
        description: description
    }).exec(function (e, r) {
        if (e) {
            return SendErrRes(res);
        } else {
            cb(r);
        }
    });
}

function create_product_details(data, req, res, cb) {
    /**
     * @description :: first route in creating a new product details. Responsible for populating the
     *                  product item table. receives json only
     */

        //collect the variables for post and validate them;
    var quantity = data.quantity;
    var details = data.details;
    var left = data.left;
    var price = data.price;
    var tbl_product = data.name;


    //check required variables
    if (!quantity || !details || !left || !tbl_product) {
        return SendErrRes(res, 'All fields are required');
    }

    //create the product and return its id
    Tbl_Product_Item.create({
        quantity: quantity,
        details: details,
        left: left,
        price: price,
        tbl_product: tbl_product,
        tbl_user: req.session.user
    }).exec(function (e, r) {
        if (e) {
            return SendErrRes(res);
        } else {
            cb(r);
        }
    });
}
