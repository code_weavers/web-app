/**
 * DashboardController
 *
 * @description :: Server-side logic for managing dashboards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var async = require('async');
module.exports = {
	index: function (req, res) {
        res.view({layout: 'dashboard_layout.ejs'});
    },

    new_commission: function (req, res) {
        var commission = req.body;
        // TODO: verfiy that the user has the permission to create that user
        create_commission(commission, res, function (r) {
            return res.ok();
        })
    },

    new_serial: function (req, res) {
        var serial = req.body;
        // TODO: verfiy that the user has the permission to create that user
        create_serial(serial, res, function (r) {
            return res.ok();
        })
    },

    get_total: function (req, res) {
        async.parallel({
            admin: function (cb) {
                Tbl_Login.count({tbl_role: 2}).exec(function (e, r) {
                    if (e)
                        cb(e);
                    else
                        cb(null, r);
                })
            },
            dist: function (cb) {
                Tbl_Login.count({tbl_role: 3}).exec(function (e, r) {

                    if (e)
                        cb(e, null);
                    else
                        cb(null, r);
                })
            },
            agent: function (cb) {
                Tbl_Login.count({tbl_role: 4}).exec(function (e, r) {

                    if (e)
                        cb(e, null);
                    else
                        cb(null, r);
                })
            },
            sales: function (cb) {
                Tbl_Login.count({tbl_role: 5}).exec(function (e, r) {

                    if (e)
                        cb(e, null);
                    else
                        cb(null, r);
                })
            }
        }, function (ee, rr) {
            if (ee) {
                console.log(ee);
                SendErrRes(res);
            } else {
                return res.jsonx(rr)
            }
        });
    },

    total_product: function (req, res) {
        async.parallel({
            product: function (cb) {
                Tbl_Product_Item.count().exec(function (e, r) {
                    if (e)
                        cb(e);
                    else
                        cb(null, r);
                })
            }
        }, function (ee, rr) {
            if (ee) {
                console.log(ee);
                SendErrRes(res);
            } else {
                return res.jsonx(rr)
            }
        });
    }

};

function create_commission(data, res, cb) {
    /**
     * @description :: first route in creating a new user. Responsible for populating the
     *                  user table. receives json only
     */

        //collect the variables for post and validate them;
    var commission = data.commission;
    var description = data.description;

    //check required variables
    if (!commission || !description) {
        return SendErrRes(res, 'All fields are required');
    }

    //create the product and return its id
    Tbl_Commission.create({
        commission: commission,
        description: description
    }).exec(function (e, r) {
        if (e) {
            console.log(e);
            return SendErrRes(res);
        } else {
            cb(r);
        }
    });
}
function create_serial(data, res, cb) {
    /**
     * @description :: first route in creating a new user. Responsible for populating the
     *                  user table. receives json only
     */

        //collect the variables for post and validate them;
    var serial = data.serial;

    //check required variables
    if (!serial) {
        return SendErrRes(res, 'All fields are required');
    }

    //create the product and return its id
    Tbl_Serial.create({
        serial: serial
    }).exec(function (e, r) {
        if (e) {
            console.log(e);
            return SendErrRes(res);
        } else {
            cb(r);
        }
    });
}
