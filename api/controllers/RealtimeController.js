/**
 * RealtimeController
 *
 * @description :: Server-side logic for managing realtimes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    notify: function (req, res) {
        if (req.isSocket) {
            Tbl_Direct_Cash.watch(req);
            Tbl_Deduction.watch(req);
            res.ok();
        }
    }
};

