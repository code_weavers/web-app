/**
 * SalesController
 *
 * @description :: Server-side logic for managing sales
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    index: function (req, res) {
        var profilepic = req.param('profilepic');
        var name = req.param('name');
        var telephone = req.param('telephone');
        var documentpic = req.param('documentpic');
        var amount_paid = req.param('total');
        var product_name = req.param('productname');
        var price = req.param('price');
        var staff_ID = req.param('staffid');
        var organisation = req.param('organisation');
        var monthly_deduction = req.param('monthly_deduction');
        var payment_type = req.param('payment_type');
        var payment_ID = req.param('payment_ID');
        var work_place = req.param('work_place');
        // sails.log.debug(req.socket);

        if (staff_ID = req.param('staffid')) {
            Tbl_Deduction.create({
                image: profilepic,
                organisation: organisation,
                staff_ID: staff_ID,
                name: name,
                telephone: telephone,
                tbl_product: product_name,
                price: price,
                monthly_deduction: monthly_deduction,
                work_place: work_place,
                doc_image: documentpic,
                tbl_user: req.session.user
            }).exec(function (e, r) {
                if (e) {
                    return SendErrRes(res);
                } else {

                    return res.json(r);
                }
            });
        } else {
            Tbl_Direct_Cash.create({
                image: profilepic,
                name: name,
                telephone: telephone,
                tbl_product: product_name,
                price: price,
                amount_paid: amount_paid,
                payment_type: payment_type,
                payment_ID: payment_ID,
                tbl_user: req.session.user

            }).exec(function (e, r) {
                if (e) {
                    return SendErrRes(res);
                } else {

                    return res.json(r);
                }
            });
        }
    },


    new_deduction: function (req, res) {
        var deduction = req.body;
        // TODO: verfiy that the user has the permission to create that user
        create_deduction(deduction, res, function (r) {
            return res.ok();
        })
    },

    new_serial: function (req, res) {
        var sales_id = req.body.salesId;
        var serial_number = req.body.serial;
        Tbl_Deduction.update({id: sales_id},{serial_number: serial_number}).exec(function afterwards(e, updated) {

            if (e) {
                console.log(e);
                return SendErrRes(res);
            } else
                console.log('Updated Serial to have Number ' + updated[0].serial_number);
                return res.ok();
        });
    },

    direct_cash: function (req, res) {
        Tbl_Direct_Cash.find().exec(function (e, r) {
            if (e) {
                return SendErrRes(res);
            } else
                return res.json(r);
        });
    },

    deduction: function (req, res) {
        Tbl_Deduction.find().exec(function (e, r) {
            if (e) {
                return SendErrRes(res);
            } else
                return res.json(r);
        });
    },

    //route for /product/details
    details: function (req, res) {
        var sales_id = req.params.id;
            console.log(sales_id);
        Tbl_Deduction.findOne({id: sales_id}).exec(function (e, r) {
                if (e) {
                    return SendErrRes(res);
                } else
                    return res.json(r);
            });
    },

    direct_cash_details: function (req, res) {
        var sales_id = req.params.id;
        Tbl_Direct_Cash.findOne({id: sales_id}).exec(function (e, r) {
            console.log(r);
            if (e) {
                return SendErrRes(res);
            } else
                return res.json(r);
        });
    },

    customer: function (req, res) {
        Tbl_Direct_Cash.find().exec(function (e, r) {
            if (e) {
                return SendErrRes(res);
            } else {
                return res.json(r);
            }

        });
    }
};

function create_deduction(data, res, cb) {
    /**
     * @description :: first route in creating a new user. Responsible for populating the
     *                  user table. receives json only
     */

        //collect the variables for post and validate them;
    var amount = data.amount;

    //check required variables
    if (!amount) {
        return SendErrRes(res, 'All fields are required');
    }

    //create the product and return its id
    Tbl_Monthly_Deduction.create({
        amount: amount
    }).exec(function (e, r) {
        if (e) {
            return SendErrRes(res);
        } else {
            cb(r);
        }
    });
}

// function create_serial(data, res, cb) {
//     /**
//      * @description :: first route in creating a new user. Responsible for populating the
//      *                  user table. receives json only
//      */
//
//         //collect the variables for post and validate them;
//     var serial = data.serial;
//
//     //check required variables
//     if (!serial) {
//         return SendErrRes(res, 'All fields are required');
//     }
//
//     //create the product and return its id
//     Tbl_Deduction.create({})({
//         serial_number: serial
//     }).exec(function (e, r) {
//         if (e) {
//             console.log(e);
//             return SendErrRes(res);
//         } else {
//             cb(r);
//         }
//     });
// }